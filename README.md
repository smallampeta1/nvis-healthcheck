# NVS-ISHEALTHY: Network Health Check Tool

Welcome to the NVS-IsHealthy project, where we test and validate health of Cumulus and Mellanox network devices. Our toolkit leverages the open-source PyATS framework to perform tests and streamline network diagnostics.

## Workflow Overview
The nvs-ishealthy health check tool follows a structured workflow, designed for simplicity and efficiency:

### Testbed Initialization:
We begin with a testbed file, which is the Single Source of Truth (SSOT) for device inventory. It includes all necessary credentials and connection details for PyATS to interface with network devices.

### Centralized Job File:
Our core nvs_ishealthy.py job file hosts multiple PyATS tests. It orchestrates the overall test execution and combines the results.

### Parallel Execution:
- Upon job initiation, PyATS simultaneously connects to all devices listed in the testbed.
- It then executes predefined test scenarios on each device. These tests are run in "execute mode" without altering their configuration.
- Outputs are parsed using TextFSM, which enables the clear presentation of PASS/FAIL results for each test.
- After testing, PyATS disconnects from all devices in parallel.

### Reporting:
At the conclusion of testing, PyATS provides comprehensive reports. These include detailed results for individual test cases and a summarized view for a quick health assessment of the network.


## Instructions to access and play with the tool

1. Create and Activate a Python Virtual Environment

 - First, create a dedicated directory for your pyATS tests and navigate into it:
```
mkdir -p ~/pyats_tests
cd ~/pyats_tests
```

 - Now, create a virtual environment named nvs_env and activate it:
```
python3 -m venv nvs_env
source nvs_env/bin/activate
```

2. Upgrade pip and setuptools
```
pip install --upgrade pip setuptools
```

3. Clone the NVS-HealthCheck Repository
```
git clone https://gitlab.com/smallampeta1/nvis-healthcheck.git
cd nvis-healthcheck/nvs-ishealthy
```

4. Install Dependencies
```
pip install -r requirements.txt
```

### Follow the below instructions to create a testbed file!

PyATS requires a testbed file with **device information (hostname, user/password, platform type, etc.)** to perform any tests.

***Note***: *It also important to make sure that all the devices in the Testbed are **reachable** or else the PyATS test would **fail**.*

***Example***: Populate the **ib_sw/eth_sw.csv** file in the inventory folder with the **IP address and device name** information. PyATS connects to the devices using the IP address, not the hostname.

![Alt text](<Screenshot 2024-03-24 at 2.49.09 PM.png>){width=50%}

Now execute the **create_testbed.py** script and follow instrutions to create the testbed file.

![Alt text](<Screenshot 2024-03-24 at 2.56.18 PM.png>){width=50%}

### Run Pyats tests using the below commands!

Now initiate PyATS test by executing ***nvs_ishealthy.py*** file and follow the instructions to run tests against a specific platform.**(Eth/IB)**

***Example***:
 - To run tests against Ethernet switches, select **ID:2** or select **Q or q** to quit.

![Alt text](<Screenshot 2024-03-24 at 3.18.06 PM.png>){width=50%}


- Now select the type of test to run.For example, selecting **ID:1** will perform ***environment checks (psu,fan,led,etc)*** and then **y or n** to proceed.

- Provide the device name(s) to run the test against, ***e.g.: P1-R30-LEAF01,P[12]-R35-LEAF.* ,P.* , .* ***

![Alt text](<Screenshot 2024-03-24 at 3.18.45 PM.png>){width=50%}

### Pyats tests reports!

After initiating the tests, reports can be viewed using the following methods:

- Each test provides live PASS/FAIL results per device.

![Alt text](<Screenshot 2024-03-24 at 3.38.24 PM.png>){width=50%}

- Test reports for all devices can be found in the pyats_reports folder in CSV format, which can later be used for verification purposes.

![Alt text](<Screenshot 2024-05-13 at 2.51.19 PM.png>){width=20%}

***Note***: Each time a test is run, the contents of the **CSV report will be overwritten.** The history of logs can be viewed using **"pyats logs view",** which will open a UI.

![Alt text](<Screenshot 2024-03-24 at 3.46.04 PM.png>){width=50%}

- PyATS provides a summary report at the end of the tests.

![Alt text](<Screenshot 2024-03-24 at 3.39.44 PM.png>){width=50%}




