import os
import pandas as pd
from openpyxl import load_workbook
from openpyxl.drawing.image import Image
from openpyxl.chart import BarChart, LineChart, PieChart
from openpyxl.styles import Font, Alignment
from openpyxl.utils import get_column_letter
from rich.console import Console
from rich.prompt import Prompt
from rich.table import Table

def is_valid_excel(file_path):
    try:
        load_workbook(file_path)
        return True
    except Exception:
        return False

def combine_excel_files(input_folder, output_file):
    console = Console()
    all_dataframes = {}
    excel_files = [f for f in os.listdir(input_folder) if f.endswith('.xlsx')]

    if not excel_files:
        console.print(f"[bold red]No Excel files found in the folder: {input_folder}[/bold red]")
        return

    console.print(f"[bold green]Found {len(excel_files)} Excel files. Combining data...[/bold green]")

    for file in excel_files:
        file_path = os.path.join(input_folder, file)
        if not is_valid_excel(file_path):
            console.print(f"[red]Skipping invalid Excel file: {file}[/red]")
            continue

        workbook = pd.ExcelFile(file_path)
        for sheet_name in workbook.sheet_names:
            if sheet_name not in all_dataframes:
                all_dataframes[sheet_name] = []
            df = workbook.parse(sheet_name)
            all_dataframes[sheet_name].append(df)

    with pd.ExcelWriter(output_file, engine='openpyxl') as writer:
        for sheet_name, df_list in all_dataframes.items():
            combined_df = pd.concat(df_list, ignore_index=True)
            combined_df.to_excel(writer, sheet_name=sheet_name, index=False, startrow=0, startcol=0)

        dest_workbook = writer.book

        # Remove existing Dashboard if it exists
        if 'Dashboard' in dest_workbook.sheetnames:
            std = dest_workbook['Dashboard']
            dest_workbook.remove(std)

        # Create the Dashboard sheet
        dashboard_ws = dest_workbook.create_sheet(title='Dashboard', index=0)

        # Add a title to the Dashboard
        title = "Device Health Stats"
        dashboard_ws.merge_cells('N1:Y1')
        title_cell = dashboard_ws['N1']
        title_cell.value = title
        title_cell.font = Font(size=25, bold=True)
        title_cell.alignment = Alignment(horizontal='center')

        row_offset = 3  # Start a few rows below the title
        col_offset = 2  # Start from the second column for some left padding
        max_columns = 3  # Number of graphs per row
        chart_width = 10  # Width in Excel columns
        chart_height = 20  # Height in Excel rows

        for file in excel_files:
            file_path = os.path.join(input_folder, file)
            if not is_valid_excel(file_path):
                continue

            src_workbook = load_workbook(file_path)

            for sheet_name in src_workbook.sheetnames:
                src_sheet = src_workbook[sheet_name]
                dest_sheet = dest_workbook[sheet_name]

                # Copy column widths from source to destination sheet
                for col in range(src_sheet.max_column):
                    dest_sheet.column_dimensions[get_column_letter(col + 1)].width = src_sheet.column_dimensions[get_column_letter(col + 1)].width

                # Clone images to the destination sheet, preserving their location and size
                for drawing in src_sheet._images:
                    new_image = Image(drawing.ref)
                    dest_sheet.add_image(new_image, drawing.anchor)

                # Clone charts to the destination sheet, preserving their location and size
                for chart in src_sheet._charts:
                    new_chart = type(chart)()
                    for s in chart.series:
                        new_chart.series.append(s)
                    new_chart.title = chart.title
                    if hasattr(chart, 'y_axis'):
                        new_chart.y_axis.title = chart.y_axis.title
                    if hasattr(chart, 'x_axis'):
                        new_chart.x_axis.title = chart.x_axis.title
                    dest_sheet.add_chart(new_chart, chart.anchor)

                # Clone images and charts to the Dashboard, preserving their size
                for drawing in src_sheet._images:
                    new_image = Image(drawing.ref)
                    dashboard_ws.add_image(new_image, f'{get_column_letter(col_offset)}{row_offset}')
                    col_offset += chart_width  # Adjust column offset for images
                    if col_offset > max_columns * chart_width:
                        col_offset = 2  # Reset to the second column for left padding
                        row_offset += chart_height

                for chart in src_sheet._charts:
                    new_chart = type(chart)()
                    for s in chart.series:
                        new_chart.series.append(s)
                    new_chart.title = chart.title
                    if hasattr(chart, 'y_axis'):
                        new_chart.y_axis.title = chart.y_axis.title
                    if hasattr(chart, 'x_axis'):
                        new_chart.x_axis.title = chart.x_axis.title
                    dashboard_ws.add_chart(new_chart, f'{get_column_letter(col_offset)}{row_offset}')

                    col_offset += chart_width  # Adjust column offset for charts
                    if col_offset > max_columns * chart_width:
                        col_offset = 2  # Reset to the second column for left padding
                        row_offset += chart_height

    console.print(f"[bold green]Final report created: {output_file}[/bold green]")

def main():
    console = Console()
    console.print("[bold green]Welcome to the Final Report Generator![/bold green]")
    console.print()  # Empty line for spacing

    device_options = {
        '1': 'IB switch',
        '2': 'Ethernet switch',
        '3': 'DGX/HGX Host'
    }
    table = Table(title="Device Types", show_header=True, header_style="bold magenta")
    table.add_column("Option")
    table.add_column("Type")
    for option, device_type in device_options.items():
        table.add_row(option, device_type)
    console.print(table)

    choice = Prompt.ask("Select the type of device (1 for IB switch, 2 for Ethernet switch, 3 for Host, or q to quit)", choices=["1", "2", "3", "q"])

    if choice.lower() == 'q':
        console.print("[bold yellow]Exiting...[/bold yellow]")
        return

    if choice == '2':
        input_folder = './pyats_reports/eth_switch/'
        output_file = './pyats_reports/eth_switch/final_report.xlsx'
        try:
            combine_excel_files(input_folder, output_file)
        except Exception as e:
            console.print(f"[red]Failed to create the final report: {e}[/red]")
    else:
        console.print(f"[bold yellow]Support for the selected device type is not yet implemented.[/bold yellow]")

if __name__ == "__main__":
    main()
