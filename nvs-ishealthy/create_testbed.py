import csv
import yaml
from rich.console import Console
from rich.prompt import Prompt
from rich.table import Table

def csv_to_yaml(csv_file, yaml_file, os_type, credentials):
    data = {
        'testbed': {
            'name': f"{os_type.capitalize()}_Testbed",
            'credentials': {
                'default': {
                    'username': credentials['username'],
                    'password': credentials['password']
                }
            }
        },
        'devices': {}
    }

    with open(csv_file, mode='r') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            device_name = row['device_name']
            ip_address = row['ip_address']
            device_info = {
                'os': os_type,
                'type': 'linux',
                'connections': {
                    'cli': {
                        'protocol': 'ssh',
                        'ip': ip_address,
                        'arguments':{
                            'connection_timeout': 5
                        },
                    }
                }
            }
            data['devices'][device_name] = device_info

    with open(yaml_file, mode='w') as file:
        file.write('---\n')
        yaml.safe_dump(data, file, sort_keys=False, default_flow_style=False)

def main():
    console = Console()
    console.print("[bold green]Welcome to the Testbed File Generator![/bold green]")
    console.print()  # Empty line for spacing

    switch_options = {
        '1': 'IB switch',
        '2': 'Ethernet switch',
        '3': 'DGX/HGX Host'
    }
    table = Table(title="Device Types", show_header=True, header_style="bold magenta")
    table.add_column("Option")
    table.add_column("Type")
    for option, switch_type in switch_options.items():
        table.add_row(option, switch_type)
    console.print(table)

    choice = Prompt.ask("Select the type of device (1 for IB switch, 2 for Ethernet switch, 3 for Host, or q to quit)", choices=["1", "2", "3", "q"])

    if choice.lower() == 'q':
        console.print("[bold yellow]Exiting...[/bold yellow]")
        return

    csv_file_map = {
        '1': ('inventory/ib_sw_inv.csv', 'testbed/ib-sw-testbed.yaml', 'mellanox'),
        '2': ('inventory/eth_sw_inv.csv', 'testbed/eth-sw-testbed.yaml', 'cumulus'),
        '3': ('inventory/host_inv.csv', 'testbed/host-testbed.yaml', 'linux')
    }
    csv_file, yaml_file, os_type = csv_file_map[choice]

    username = Prompt.ask("Enter the default username")
    password = Prompt.ask("Enter the default password", password=True)

    credentials = {'username': username, 'password': password}

    try:
        csv_to_yaml(csv_file, yaml_file, os_type, credentials)
        console.print(f"[bold green]Testbed file created at {yaml_file}[/bold green]")
    except Exception as e:
        console.print(f"[red]Failed to create testbed file: {e}[/red]")

if __name__ == "__main__":
    main()
