from rich.console import Console
from rich.prompt import Confirm
from rich.table import Table
from rich.panel import Panel
from rich.text import Text


# Initialize the rich console
console = Console()


def display_welcome_banner():
    welcome_text = Text()
    welcome_text.append("✨✨✨ ", style="bold bright_yellow")
    welcome_text.append("Welcome to the NVS Healthcheck Tool ", style="bold green")
    welcome_text.append("✨✨✨", style="bold bright_yellow")
    console.print(Panel(welcome_text, expand=False, border_style="bold green"))

def display_platform_options():
    table = Table(show_header=True, header_style="bold magenta")
    table.add_column("ID", style="white", width=4)
    table.add_column("Platform", style="white", min_width=20)
    #table.add_row("1", "IB Switch")
    table.add_row("2", "Ethernet Switch")
    table.add_row("3", "DGX/HGX Host")
    table.add_row("Q", "Quit")
    console.print(Panel(table, title="[bold yellow]Select a Platform to Perform Tests[/bold yellow]", expand=False))

def display_test_options(platform_name):
    if platform_name == 'IB Switch':
        tests = [
            #("1", "Environment Check", "Verifies the system environment."),
            #("2", "Interface Diagnostics", "Runs diagnostics on network interfaces."),
            #("3", "Config Backup", "Performs config backup of network devices.")
        ]
        test_script_map = {
            #'1': 'ib_switch/env_check.py',
            #'2': 'ib_switch/intf_diag.py',
            #'3': 'ib_switch/config_backup.py'
        }
    elif platform_name == 'Ethernet Switch':
        tests = [
            #("1", "Environment Check", "Verifies the device cpu, fan, psu, etc."),
            #("2", "Underlay Check", "Verifies the status of the underlay network, e.g: bgp"),
            ("3", "Intf stats", "Verifies interfaces checks - Miss cabling, BER, Bond interfaces etc.")
            #("5", "Config Backup", "Performs config backup of network devices.")
        ]
        test_script_map = {
            #'1': 'eth_switch/env_check.py', 
            #'2': 'eth_switch/underlay_check.py',
            '3': None  # Set to None to redirect to the sub-options
            #'5': 'eth_switch/config_backup.py'
        }
    elif platform_name == 'DGX/HGX Host':
        tests = [
            ("1", "Port Checker", "Performs [A-Z] LLDP neighbor check for DGX/HGX hosts."),
            #("2", "Point to Point Ping test", "Performs ping tests on DGX/HGX host P2P /31 interfaces."),
        ]
        test_script_map = {
            '1': 'host/port_checker.py',
            #'2': 'host/ping_test.py',
        }
    else:
        console.print("[bold red]Invalid platform choice. Exiting...[/bold red]")
        sys.exit()

    table = create_test_table(tests, platform_name)
    console.print(Panel(table, title=f"[bold yellow]Available Tests for {platform_name}[/bold yellow]", expand=False))
    return test_script_map

def display_intf_stats_options():
    tests = [
        #("1", "Intf Bond", "Performs bond interfaces check on a network device."),
        #("2", "Intf BER", "Checks for interface BER stats on a network device."),
        ("3", "Port Checker", "Performs [A-Z] LLDP neighbor check of a network device."),
        #("4", "Detailed Port Checker", "Performs a detailed LLDP neighbor check of a network device.")
    ]
    test_script_map = {
        #'1': 'eth_switch/intf_bond.py',
        #'2': 'eth_switch/intf_ber.py',
        '3': 'eth_switch/port_checker.py',
        #'4': 'eth_switch/port_checker_detail.py'
    }

    table = create_test_table(tests, "Intf stats")
    console.print(Panel(table, title=f"[bold yellow]Available Interface Stats Tests[/bold yellow]", expand=False))
    return test_script_map

def create_test_table(tests, platform):
    table = Table(show_header=True, header_style="bold magenta")
    table.add_column("ID", style="white", width=4)
    table.add_column("Test", style="white", min_width=20)
    table.add_column("Description", style="white", min_width=40)
    for test_id, test_name, description in tests:
        table.add_row(test_id, test_name, description)
    table.add_row("Q", "Quit", "Exit the program.")
    return table


def display_health_checks(platform):
    console = Console()
    table = Table(show_header=True, title="Health Checks to be Performed", header_style="bold magenta")

    # Define health checks for each platform
    health_checks = {
        'Ethernet Switch': [
            ("PSU", "Verify the status of Power Supply Unit."),
            ("FAN", "Verify the status of FAN modules."),
            ("LED", "Verify the status of LED's."),
            ("Sensor", "Verify the status of Sensor's.")
        ],
        'IB Switch': [
            ("PSU", "Verify the status of Power Supply Unit."),
            ("FAN", "Verify the status of FAN modules."),
            ("LED", "Verify the status of LED's."),
            ("Voltage", "Verify the status of Voltage.")
        ]
    }

    checks = health_checks.get(platform, [])
    
    if not checks:
        console.print(f"[bold red]No health checks defined for the platform {platform}.[/bold red]")
        console.print()
        return

    table.add_column("Check", style="white", no_wrap=True)
    table.add_column("Description", style="white")

    for check, description in checks:
        table.add_row(check, description)

    console.print(table)

def ask_to_proceed():
    proceed = Confirm.ask("===> Do you want to proceed with the health checks?")
    console.print()
    return proceed