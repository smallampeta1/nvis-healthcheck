from rich.prompt import Prompt
from rich.console import Console
from rich.panel import Panel
from genie.testbed import load
from functions import *
import os
import re
import logging
import sys
import time

# Initialize the rich console
console = Console()

# Set up the logger globally
logging.getLogger('pyats').setLevel(logging.WARNING)
logging.getLogger('unicon').setLevel(logging.WARNING)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class DeviceInfo:
    def __init__(self, device):
        self.device = device
        self.os_type = device.os
        self.device_type = device.type
        self.protocol = device.connections.cli.protocol
        self.ip = device.connections.cli.ip

def main(runtime):
    runtime.no_archive = True
    
    display_welcome_banner()

    # Mapping user choices to testbed filenames
    testbed_files = {
        '1': 'testbed/ib-sw-testbed.yaml',
        '2': 'testbed/eth-sw-testbed.yaml',
        '3': 'testbed/host-testbed.yaml'
    }
    
    # Mapping platform choices to names
    platform_names = {'1': 'IB Switch', '2': 'Ethernet Switch', '3': 'DGX/HGX Host'}
    
    display_platform_options()
    platform_choice = Prompt.ask("===> Please select a platform by entering the ID or [bold red]Q[/bold red] to quit", choices=[
        #"1", 
        "2", "3", "Q", "q"])
    console.print()

    if platform_choice.lower() == 'q':
        console.print("\n[bold red]Exiting...NVS_ISHealthy Tool!!![/bold red]\n")
        console.print()
        sys.exit()
    
    testbed_filename = testbed_files.get(platform_choice)
    if not testbed_filename:
        console.print("\n[bold red]Invalid platform selection. Exiting...[/bold red]\n")
        sys.exit()
    
    testbed = load(os.path.join(os.path.dirname(__file__), testbed_filename))
    
    platform_name = platform_names.get(platform_choice, None)
    test_script_map = display_test_options(platform_name)
    
    test_choice = Prompt.ask("===> Please select a test by entering the ID or [bold red]Q[/bold red] to quit", choices=list(test_script_map.keys()) + ["Q", "q"])
    console.print()

    if test_choice.lower() == 'q':
        console.print("\n[bold red]Exiting...NVS_ISHealthy Tool!!![/bold red]\n")
        console.print()
        sys.exit()
    
    # Check if the selected test is 'Intf stats' (option 3) for Ethernet Switch
    if platform_name == 'Ethernet Switch' and test_choice == '3':
        intf_stats_map = display_intf_stats_options()
        intf_choice = Prompt.ask("===> Please select an interface stat test by entering the ID or [bold red]Q[/bold red] to quit", choices=list(intf_stats_map.keys()) + ["Q", "q"])
        console.print()

        if intf_choice.lower() == 'q':
            console.print("\n[bold red]Exiting...NVS_ISHealthy Tool!!![/bold red]\n")
            console.print()
            sys.exit()

        selected_test_script = intf_stats_map.get(intf_choice)
    else:
        selected_test_script = test_script_map.get(test_choice)

    if not selected_test_script:
        logger.error("Invalid test choice. Exiting.")
        sys.exit()
    
    # Skip health checks for DGX/HGX Host
    if platform_name != 'DGX/HGX Host' and test_choice == '1':
        display_health_checks(platform_name)
        if not ask_to_proceed():
            console.print("\n[bold red]Exiting...NVS_ISHealthy Tool!!![/bold red]\n")
            console.print()
            sys.exit()

    device_regex_input = Prompt.ask("===> Please enter device regex to filter by name or 'all' for all devices(e.g.,'P1-R[12]-LEAF.*')")
    console.print()

    filtered_devices = {}  # Initialize to ensure it's always defined

    # Handle 'all' case or '.*'
    if device_regex_input.strip().lower() == 'all' or device_regex_input.strip() == '.*':
        filtered_devices = testbed.devices  # Assign all devices when all devices are to be connected
    else:
        # Transform comma-separated list into a regex pattern with OR operator
        device_regex_patterns = [s.strip() for s in device_regex_input.split(',')]
        device_regex = '|'.join(f"^{pattern}$" for pattern in device_regex_patterns)
        pattern = re.compile(device_regex)
        filtered_devices = {name: device for name, device in testbed.devices.items() if pattern.search(name)}

    if not filtered_devices:
        logger.error("\nNo devices matched the given regex. Exiting.\n")
        exit(1)

    # Create DeviceInfo objects for filtered devices
    device_infos = [DeviceInfo(device) for name, device in filtered_devices.items()]

    if selected_test_script:
        testscript_path = os.path.join(os.path.dirname(__file__), f'pyats_tests/{selected_test_script}')
        console.print(Panel(f"Running [bold]{selected_test_script}[/bold]...", style="bold cyan"))

        device_count = len(filtered_devices)
        patience_message = f"Connecting to {device_count} device{'s' if device_count > 1 else ''}. Please be patient while we connect..."
        console.print(Panel(patience_message, style="bold yellow"))

        # Start timing
        start_time = time.time()

        runtime.tasks.run(testscript=testscript_path, device_infos=device_infos)

        # End timing
        end_time = time.time()
        elapsed_time = end_time - start_time

        # Convert elapsed time to hours, minutes, seconds
        hours, rem = divmod(elapsed_time, 3600)
        minutes, seconds = divmod(rem, 60)

        console.print(Panel("[bold green]Test execution complete! Thank you for using the tool.[/bold green]", expand=False))
        console.print(f"[bold yellow]Time taken: {int(hours)}h:{int(minutes)}m:{int(seconds)}s[/bold yellow]")    
    else:
        logger.error("Invalid test choice. Exiting.")
        sys.exit()

if __name__ == "__main__":
    main()