from netmiko import ConnectHandler
from getpass import getpass
from rich.console import Console
from rich.table import Table
import csv
from io import StringIO



def read_device_info(filename):
    """Read device information from a CSV file."""
    devices = []
    with open(filename, mode='r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            devices.append(row)
    return devices


# Adjusted SSH Command Execution Function to use Netmiko
def ssh_command_with_netmiko(device_type, ip_address, username, password, command):
    """Execute an SSH command using Netmiko and return the output."""
    device = {
        'device_type': device_type,
        'host': ip_address,
        'username': username,
        'password': password,
        'use_keys': False,
        'auth_timeout': 10,
    }

    with ConnectHandler(**device) as net_connect:
        net_connect.enable()  # Automatically enters enable mode
        output = net_connect.send_command(command)
    return output

# Configuration Comparison Functions
def compare_configs(actual_configs, intended_configs):
    missing_configs = [config for config in intended_configs if config not in actual_configs]
    extra_configs = [config for config in actual_configs if config not in intended_configs]
    return missing_configs, extra_configs

def display_config_diffs(device_name, missing_configs, extra_configs):
    # Console for terminal output
    console = Console()
    # Console for capturing output in a buffer
    buffer = StringIO()
    file_console = Console(file=buffer, record=True)
    
    # Modified to accept arbitrary args and kwargs
    def print_and_write(*args, **kwargs):
        console.print(*args, **kwargs)
        file_console.print(*args, **kwargs)
    
    missing_title = f"Missing Configurations for {device_name}"
    extra_title = f"Extra Configurations for {device_name}"
    
    # For Missing Configurations
    if missing_configs:
        table = Table(title=missing_title, header_style="bold yellow")
        table.add_column("Configuration")
        for config in missing_configs:
            table.add_row(config, style="red")  # Use style here
        print_and_write(table)  # Print and write the table
    else:
        print_and_write(f"No missing configurations for {device_name}!", style="bold green")

    # For Extra Configurations
    if extra_configs:
        table = Table(title=extra_title, header_style="bold yellow")
        table.add_column("Configuration")
        for config in extra_configs:
            table.add_row(config, style="green")  # Use style here
        print_and_write(table)  # Print and write the table
    else:
        print_and_write(f"No extra configurations for {device_name}!", style="bold green")
    
    # Write buffer content to file after processing all outputs
    with open("config_diffs_output.txt", "a") as file:
        file.write(buffer.getvalue())

def main():
    username = input(f"Enter SSH username for all devices: ")
    password = getpass("Enter SSH password for all devices: ")
    devices = read_device_info('ib_devices.csv')

    devices_with_missing_configs = 0
    devices_with_all_configs = 0
    missing_or_extra_config_devices = []

    for device in devices:
        ip_address = device['ip_address']
        device_name = device['device_name']

        print(f"\nChecking configurations for {device_name} ({ip_address})...")
        
        # Use 'mellanox_ssh' or adjust based on your device. This assumes all devices are of the same type.
        device_type = 'mellanox_ssh'
        command = "show configuration"

        try:

            # Connect to the device and execute the command using Netmiko
            device_output = ssh_command_with_netmiko(device_type, ip_address, username, password, command)

            # Split the device output into lines, strip whitespace, and exclude lines not relevant to DNS or NTP settings
            actual_configs = [line.strip() for line in device_output.splitlines() if "hostname" not in line and "ntp" in line or "name-server" in line or "domain-list" in line]

            # Extending intended configurations to include NTP settings
            intended_configs = [
                "ip name-server 9.0.0.1",
                "ip name-server 9.0.0.2",
                "ip name-server 9.2.250.86",
                "ip domain-list rmf.ibm.com",
                "no ntp server 9.10.228.164 disable",
                "ntp server 9.10.228.164 keyID 0",
                "no ntp server 9.10.228.164 trusted-enable",
                "ntp server 9.10.228.164 version 4",
                "no ntp server 9.10.228.206 disable",
                "ntp server 9.10.228.206 keyID 0",
                "no ntp server 9.10.228.206 trusted-enable",
                "ntp server 9.10.228.206 version 4",
                "no ntp server 9.44.51.57 disable",
                "ntp server 9.44.51.57 keyID 0",
                "no ntp server 9.44.51.57 trusted-enable",
                "ntp server 9.44.51.57 version 4",
                "no ntp server 9.44.51.77 disable",
                "ntp server 9.44.51.77 keyID 0",
                "no ntp server 9.44.51.77 trusted-enable",
                "ntp server 9.44.51.77 version 4",
                "no ntp server 9.46.34.132 disable",
                "ntp server 9.46.34.132 keyID 0",
                "no ntp server 9.46.34.132 trusted-enable",
                "ntp server 9.46.34.132 version 4",
                "no ntp server 9.46.166.148 disable",
                "ntp server 9.46.166.148 keyID 0",
                "no ntp server 9.46.166.148 trusted-enable",
                "ntp server 9.46.166.148 version 4",
                "no ntp server 9.56.248.20 disable",
                "ntp server 9.56.248.20 keyID 0",
                "no ntp server 9.56.248.20 trusted-enable",
                "ntp server 9.56.248.20 version 4",
                "no ntp server 9.56.248.124 disable",
                "ntp server 9.56.248.124 keyID 0",
                "no ntp server 9.56.248.124 trusted-enable",
                "ntp server 9.56.248.124 version 4",
                ]

            missing_configs, extra_configs = compare_configs(actual_configs, intended_configs)
            display_config_diffs(device_name, missing_configs, extra_configs)

            if missing_configs or extra_configs:
                devices_with_missing_configs += 1
                missing_or_extra_config_devices.append((device_name, ip_address))  # Update list with issues
            else:
                devices_with_all_configs += 1

        except Exception as e:
            print(f"Failed to check {device_name}: {e}")
    # Print summary including the list of devices with issues
    print(f"\nSummary:")
    print(f"Devices with all intended configurations: {devices_with_all_configs}")
    print(f"Devices with missing or extra configurations: {devices_with_missing_configs}")

    if missing_or_extra_config_devices:
        print("\nDevices with missing or extra configurations:")
        for device_name, ip_address in missing_or_extra_config_devices:
            print(f"{device_name} ({ip_address})")
    else:
        print("All devices have the intended configurations.")


if __name__ == "__main__":
    main()