from pyats import aetest
from rich.console import Console
from rich.table import Table
import os
import re
import logging
from pyats.async_ import pcall

# Configure logging
logger = logging.getLogger(__name__)

def strip_ansi_escape_codes(string_buffer):
    """
    Remove specific ANSI escape codes from the output.
    """
    # Remove sequences like ;00m
    ansi_escape = re.compile(r'(\[\d+m|;00m)')
    return ansi_escape.sub('', string_buffer)

def connect_and_backup_config(device_info):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        output = device.execute("nv config show")
        output = strip_ansi_escape_codes(output)  # Remove ANSI escape sequences
        
        # Define the backup directory and file path
        backup_dir = f'./pyats_reports/eth_switch/config_backup/{device.name}'
        os.makedirs(backup_dir, exist_ok=True)
        backup_file_path = os.path.join(backup_dir, 'startup.yaml')
        
        # Write the output to the file
        with open(backup_file_path, 'w') as file:
            file.write(output)
        
        return {'status': 'success', 'device': device.name}
    except Exception as e:
        logger.error(f"Failed to connect or backup config on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class ConfigBackup(aetest.Testcase):
    @aetest.test
    def backup_config(self, steps, device_infos):
        """
        Backups configuration on all connected devices and outputs results.
        """
        # Use ikwargs to pass each DeviceInfo object to pcall
        responses = pcall(connect_and_backup_config, ikwargs=[{'device_info': di} for di in device_infos])
        
        console = Console()
        
        for result in responses:
            device_name = result['device']
            table = Table(show_header=True, title="Configuration Backup Status", header_style="bold magenta")
            table.add_column("Device")
            table.add_column("Status")
            
            with steps.start(f"Backup configuration on {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    table.add_row(device_name, "FAIL", style="red")
                    step.failed(f"Failed to connect or backup config on {device_name}: {result['error']}")
                else:
                    table.add_row(device_name, "PASS", style="green")
                    step.passed(f"Configuration backup completed successfully for {device_name}.")
            
            # Print the table for the current device
            console.print("\n")
            console.print(table, justify='left')

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
