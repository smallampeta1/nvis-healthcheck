from rich.console import Console
from rich.table import Table
from datetime import datetime, timedelta
import pandas as pd
import textfsm
import json
import os
import logging
from pyats import aetest
from pyats.async_ import pcall
from excel_functions.env_report import *

logger = logging.getLogger(__name__)

def connect_and_execute(device_info, command, template_file=None):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute(command)
        
        # Parse the output using textfsm if template_file is provided
        parsed_output = None
        if template_file:
            with open(template_file) as template:
                fsm = textfsm.TextFSM(template)
                parsed_output = fsm.ParseText(output)
        
        return {'status': 'success', 'device': device.name, 'output': parsed_output, 'raw_output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class DeviceHealthCheck(aetest.Testcase):
    def _verify_status(self, device_infos, steps, check_type, dataframes):
        template_dir = './templates/'
        check_data = {
            'psu': {
                'command': 'nv show platform environment psu',
                'template': 'eth_switch/nv_sh_plat_env_psu.template',
            },
            'fan': {
                'command': 'nv show platform environment fan',
                'template': 'eth_switch/nv_sh_plat_env_fan.template',
            },
            'led': {
                'command': 'nv show platform environment led',
                'template': 'eth_switch/nv_sh_plat_env_led.template',
            },
            'sensor': {
                'command': 'nv show platform environment sensor',
                'template': 'eth_switch/nv_sh_plat_env_sensor.template',
            }
        }

        check_info = check_data.get(check_type)
        if not check_info:
            return

        results = []

        # Use ikwargs to pass each DeviceInfo object to pcall
        commands = [{'device_info': di, 'command': check_info['command'], 'template_file': os.path.join(template_dir, check_info['template'])} for di in device_infos]
        responses = pcall(connect_and_execute, ikwargs=commands)

        for result in responses:
            device_name = result['device']
            with steps.start(f"Verify {check_type.capitalize()} Status on {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    results.append([device_name, '-', '-', 'Device is not reachable'])
                    step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    continue

                parsed_output = result['output']
                if not parsed_output:
                    results.append([device_name, f"No {check_type.upper()}'s found", "FAIL"])
                    step.failed(f"No {check_type}s found on {device_name}. Unable to verify {check_type} status.")
                    continue

                console = Console()
                table = Table(show_header=True, title=f"{device_name} {check_type.capitalize()} Status", header_style="bold magenta")
                table.add_column("Device")
                table.add_column(f"{check_type.capitalize()} Name")
                table.add_column(f"{check_type.capitalize()} State")
                table.add_column("Pass/Fail")
                test_failed = False

                for row in parsed_output:
                    if check_type == "sensor":
                        item_name = row[0]
                        item_state = row[4]
                        pass_fail = "PASS" if item_state.lower() == "ok" else "FAIL"
                    elif check_type == "led":
                        item_name, item_color = row
                        pass_fail = "PASS" if item_color.lower() == "green" else "FAIL"
                    else:
                        item_name = row[0]
                        item_state = row[-1]
                        pass_fail = "PASS" if item_state.lower() == "ok" else "FAIL"

                    row_style = "green" if pass_fail == "PASS" else "red"

                    if pass_fail == "FAIL":
                        test_failed = True

                    results.append([device_name, item_name, item_color if check_type == "led" else item_state, pass_fail])
                    table.add_row(device_name, item_name, item_color if check_type == "led" else item_state, pass_fail, style=row_style)

                console.print("\n")
                console.print(table, justify='left')

                if test_failed:
                    step.failed(f"{check_type.capitalize()} check failed on {device_name}. See table above for details.")
                else:
                    step.passed(f"All {check_type}s on {device_name} are OK")

        # Store results in the dataframes dictionary
        dataframes[check_type] = pd.DataFrame(results, columns=["Device", f"{check_type.capitalize()} Name", f"{check_type.capitalize()} State", "Pass/Fail"])

    @aetest.test
    def verify_hardware_status(self, steps, device_infos):
        command = 'nv show platform hardware -o json'
        results = []

        # Use ikwargs to pass each DeviceInfo object to pcall
        commands = [{'device_info': di, 'command': command} for di in device_infos]
        responses = pcall(connect_and_execute, ikwargs=commands)

        for result in responses:
            device_name = result['device']
            with steps.start(f"Verify Hardware Status on {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    results.append([device_name, 'Device is not reachable', '-', '-', '-', '-'])
                    step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    continue

                hardware_info = json.loads(result['raw_output'])

                # Append results to the list
                results.append([
                    device_name, 
                    hardware_info.get('model', 'N/A'), 
                    hardware_info.get('part-number', 'N/A'), 
                    hardware_info.get('serial-number', 'N/A'), 
                    hardware_info.get('system-mac', 'N/A'), 
                    hardware_info.get('port-layout', 'N/A')
                ])

                # Print in a table format
                console = Console()
                table = Table(show_header=True, title=f"{device_name} Hardware Information", header_style="bold magenta")
                table.add_column("Attribute")
                table.add_column("Value")
                for key, value in hardware_info.items():
                    table.add_row(key, str(value))
                console.print("\n")
                console.print(table)

                step.passed(f"Hardware information collected for {device_name}")

        # Store results in the dataframes dictionary
        self.parent.parameters['dataframes']['hardware'] = pd.DataFrame(results, columns=["Device", "Model", "Part Number", "Serial Number", "System MAC", "Port Layout"])

    @aetest.test
    def verify_system_status(self, steps, device_infos):
        command_system = 'nv show system -o json'
        command_cpu = 'nv show system cpu -o json'
        command_memory = 'nv show system memory -o json'
        results = []

        # Collect commands for each device
        commands_system = [{'device_info': di, 'command': command_system} for di in device_infos]
        commands_cpu = [{'device_info': di, 'command': command_cpu} for di in device_infos]
        commands_memory = [{'device_info': di, 'command': command_memory} for di in device_infos]

        # Execute commands in parallel
        responses_system = pcall(connect_and_execute, ikwargs=commands_system)
        responses_cpu = pcall(connect_and_execute, ikwargs=commands_cpu)
        responses_memory = pcall(connect_and_execute, ikwargs=commands_memory)

        for i, device_info in enumerate(device_infos):
            device_name = device_info.device.name
            with steps.start(f"Collecting System, CPU, and Memory Status on {device_name}", continue_=True) as step:
                if responses_system[i]['status'] == 'failed' or responses_cpu[i]['status'] == 'failed' or responses_memory[i]['status'] == 'failed':
                    results.append([device_name, 'Device is not reachable', '-', '-', '-', '-', '-'])
                    step.failed(f"Failed to connect or execute command on {device_name}")
                    continue

                try:
                    # Parse system information
                    system_data = json.loads(responses_system[i]['raw_output'])
                    uptime_seconds = int(system_data['uptime'])
                    uptime_str = str(timedelta(seconds=uptime_seconds))

                    # Parse CPU information
                    cpu_data = json.loads(responses_cpu[i]['raw_output'])
                    cpu_utilization = f"{round(cpu_data['utilization'])}%"

                    # Parse memory information
                    memory_data = json.loads(responses_memory[i]['raw_output'])
                    physical_memory_utilization = f"{round(memory_data['Physical']['utilization'])}%"

                    # Append results to the list
                    results.append([device_name, system_data['build'], uptime_str, cpu_data['model'], cpu_data['core-count'], cpu_utilization, physical_memory_utilization])

                    # Print in a table format using Rich
                    console = Console()
                    table = Table(show_header=True, title=f"{device_name} System, CPU, and Memory Information", header_style="bold magenta")
                    table.add_column("Attribute")
                    table.add_column("Value")
                    table.add_row("Build", system_data['build'])
                    table.add_row("Uptime", uptime_str)
                    table.add_row("CPU Model", cpu_data['model'])
                    table.add_row("Core Count", str(cpu_data['core-count']))
                    table.add_row("CPU Utilization", cpu_utilization)
                    table.add_row("Physical Memory Utilization", physical_memory_utilization)
                    console.print("\n")
                    console.print(table)

                    step.passed("System, CPU, and memory information collected successfully")

                except json.JSONDecodeError:
                    step.failed("Failed to parse JSON output.")
                except KeyError as e:
                    step.failed(f"Missing expected key in JSON output: {str(e)}")
                except Exception as e:
                    step.failed(f"An error occurred: {str(e)}")

        # Store results in the dataframes dictionary
        self.parent.parameters['dataframes']['system'] = pd.DataFrame(results, columns=["Device", "Build", "Uptime", "CPU Model", "Core Count", "CPU Utilization", "Physical Memory Utilization"])

    @aetest.test
    def verify_psu_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'psu', self.parent.parameters['dataframes'])

    @aetest.test
    def verify_fan_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'fan', self.parent.parameters['dataframes'])

    @aetest.test
    def verify_led_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'led', self.parent.parameters['dataframes'])

    @aetest.test
    def verify_sensor_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'sensor', self.parent.parameters['dataframes'])

    @aetest.cleanup
    def create_env_report(self):
        dataframes = self.parent.parameters['dataframes']
        report_file_path = './pyats_reports/eth_switch/device_health_check_report.xlsx'
        create_env_report(dataframes, report_file_path, report_type="device_health")
    
    @aetest.setup
    def setup(self):
        # Initialize an empty dictionary to store dataframes
        self.parent.parameters['dataframes'] = {}

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
