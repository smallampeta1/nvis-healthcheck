import pandas as pd
import datetime

def calculate_major_unit(max_value):
    """Calculate an appropriate major unit for the y-axis based on the maximum value."""
    if max_value <= 10:
        return 1
    elif max_value <= 100:
        return 10
    elif max_value <= 1000:
        return 100
    else:
        return 500

def create_env_report(dataframes, report_file_path, report_type):
    report_date_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    with pd.ExcelWriter(report_file_path, engine='xlsxwriter') as writer:
        for check_type, df in dataframes.items():
            df.to_excel(writer, sheet_name=check_type.capitalize(), index=False)
            worksheet = writer.sheets[check_type.capitalize()]

            # Adjust column widths
            for i, column in enumerate(df.columns):
                column_width = max(df[column].astype(str).map(len).max(), len(column)) + 2
                worksheet.set_column(i, i, column_width)

            if report_type == "device_health":
                workbook = writer.book

                if check_type == "hardware":
                    total_items = len(df)
                    model_counts = df['Model'].value_counts()

                    summary_df = pd.DataFrame({'Category': ['Total'] + model_counts.index.tolist(),
                                               'Count': [total_items] + model_counts.tolist()})
                    summary_start_row = len(df) + 2

                    summary_df.to_excel(writer, sheet_name=check_type.capitalize(), startrow=summary_start_row, index=False)

                    chart = workbook.add_chart({'type': 'column'})
                    chart.add_series({
                        'name': 'Hardware Summary',
                        'categories': [check_type.capitalize(), summary_start_row + 1, 0, summary_start_row + len(summary_df), 0],
                        'values': [check_type.capitalize(), summary_start_row + 1, 1, summary_start_row + len(summary_df), 1],
                        'data_labels': {'value': True}  # Show value labels on top of each bar
                    })
                    chart.set_title({'name': f'Hardware Summary (Report Date: {report_date_time})'})
                    chart.set_x_axis({'name': 'Category'})
                    chart.set_y_axis({
                        'name': 'Count',
                        'major_gridlines': {'visible': False},
                        'major_unit': calculate_major_unit(summary_df['Count'].max())
                    })
                    chart.set_size({'width': 800, 'height': 500})
                    worksheet.insert_chart('L10', chart)

                elif check_type == "system":
                    build_counts = df['Build'].value_counts()

                    summary_df = pd.DataFrame({'Build': build_counts.index, 'Count': build_counts.values})
                    summary_start_row = len(df) + 2

                    summary_df.to_excel(writer, sheet_name=check_type.capitalize(), startrow=summary_start_row, index=False)

                    chart = workbook.add_chart({'type': 'pie'})
                    chart.add_series({
                        'name': 'System Build Version Distribution',
                        'categories': [check_type.capitalize(), summary_start_row + 1, 0, summary_start_row + len(summary_df), 0],
                        'values': [check_type.capitalize(), summary_start_row + 1, 1, summary_start_row + len(summary_df), 1],
                        'data_labels': {'value': True}  # Show counts instead of percentages
                    })
                    chart.set_title({'name': f'System Build Version Distribution (Report Date: {report_date_time})'})
                    chart.set_size({'width': 800, 'height': 500})
                    worksheet.insert_chart('L10', chart)

                elif check_type in ["psu", "fan", "led", "sensor"]:
                    status_column = df.columns[-1]
                    filtered_df = df[df[status_column].isin(['PASS', 'FAIL'])]
                    total_items = len(filtered_df)
                    state_counts = filtered_df[status_column].value_counts()

                    non_reachable_count = len(df[df[status_column] == 'Device is not reachable'])

                    summary_df = pd.DataFrame({'Category': ['Total'] + state_counts.index.tolist() + ['Device\nNon-Reachable'],
                                               'Count': [total_items] + state_counts.tolist() + [non_reachable_count]})
                    summary_start_row = len(df) + 2

                    summary_df.to_excel(writer, sheet_name=check_type.capitalize(), startrow=summary_start_row, index=False)

                    chart = workbook.add_chart({'type': 'column'})
                    points = [{'fill': {'color': '#0000FF'}},  # Blue for Total
                              {'fill': {'color': '#006400'}},  # Dark Green for Pass
                              {'fill': {'color': '#FFA500'}},  # Orange for Fail
                              {'fill': {'color': '#FF0000'}}]  # Red for Device Non-Reachable
                    chart.add_series({
                        'name': f'{check_type.capitalize()} Summary',
                        'categories': [check_type.capitalize(), summary_start_row + 1, 0, summary_start_row + len(summary_df), 0],
                        'values': [check_type.capitalize(), summary_start_row + 1, 1, summary_start_row + len(summary_df), 1],
                        'points': points[:len(summary_df)],
                        'data_labels': {'value': True}  # Show value labels on top of each bar
                    })
                    chart.set_title({'name': f'{check_type.capitalize()} Summary (Report Date: {report_date_time})'})
                    chart.set_x_axis({'name': 'Category'})
                    chart.set_y_axis({
                        'name': 'Count',
                        'major_gridlines': {'visible': False},
                        'major_unit': calculate_major_unit(summary_df['Count'].max())
                    })
                    chart.set_size({'width': 800, 'height': 500})
                    worksheet.insert_chart('L10', chart)