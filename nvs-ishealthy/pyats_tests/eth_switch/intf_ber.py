import json
import os
from datetime import datetime
from pyats import aetest
from pyats.topology import loader
from pyats.async_ import pcall
from rich.console import Console
from rich.table import Table
import pandas as pd
from openpyxl import load_workbook, Workbook
from openpyxl.chart import BarChart, Reference, Series
from openpyxl.utils.dataframe import dataframe_to_rows
import logging

logger = logging.getLogger(__name__)
console = Console()

# Define snapshot parameters
SNAPSHOT_FOLDER = './pyats_reports/eth_switch/snapshot'
EXCEL_REPORT_PATH = './pyats_reports/eth_switch/interface_counters_comparison.xlsx'

def connect_and_execute(device_info, command):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute(command)
        
        return {'status': 'success', 'device': device.name, 'output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class InterfaceCountersTestcase(aetest.Testcase):
    @aetest.test
    def take_snapshots(self, device_infos, steps):
        # Ensure the snapshot folder exists
        os.makedirs(SNAPSHOT_FOLDER, exist_ok=True)

        commands = [{'device_info': di, 'command': 'nv show interface counters -o json'} for di in device_infos]
        responses = pcall(connect_and_execute, ikwargs=commands)

        results = []
        for result in responses:
            device_name = result['device']
            with steps.start(f"Taking snapshot for {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    continue

                if not result['output'].strip():
                    step.failed(f"No output received from {device_name}.")
                    continue

                try:
                    interface_data = json.loads(result['output'])
                except json.JSONDecodeError:
                    step.failed(f"Failed to parse JSON output from {device_name}.")
                    continue

                device_snapshot_folder = os.path.join(SNAPSHOT_FOLDER, device_name)
                os.makedirs(device_snapshot_folder, exist_ok=True)

                timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
                snapshot_file = f"{device_name}_snapshot_{timestamp}.json"
                snapshot_path = os.path.join(device_snapshot_folder, snapshot_file)

                try:
                    with open(snapshot_path, 'w') as f:
                        json.dump(interface_data, f, indent=2)
                    results.append([device_name, snapshot_file, "Success"])
                    step.passed(f"Snapshot saved to {snapshot_path}")
                except Exception as e:
                    results.append([device_name, snapshot_file, f"Failed: {e}"])
                    step.failed(f"Failed to save snapshot for {device_name} to {snapshot_path}: {e}")

        # Print results in a table using Rich
        table = Table(show_header=True, title="Snapshot Results", header_style="bold magenta")
        table.add_column("Device")
        table.add_column("Snapshot File")
        table.add_column("Status")

        for row in results:
            row_style = "green" if "Success" in row[2] else "red"
            table.add_row(*[str(cell) for cell in row], style=row_style)

        console.print("\n")
        console.print(table, justify='left')

    @aetest.test
    def compare_snapshots(self, device_infos, steps):
        differences_found = False
        all_data = []

        for device_info in device_infos:
            device_name = device_info.device.name
            device_snapshot_folder = os.path.join(SNAPSHOT_FOLDER, device_name)
            os.makedirs(device_snapshot_folder, exist_ok=True)
            snapshot_files = sorted(
                [f for f in os.listdir(device_snapshot_folder) if f.endswith('.json')],
                reverse=True
            )
            if len(snapshot_files) < 2:
                console.print(f"Not enough snapshots for {device_name} to compare. Skipping.")
                continue

            snapshot2_path = os.path.join(device_snapshot_folder, snapshot_files[0])
            snapshot1_path = os.path.join(device_snapshot_folder, snapshot_files[1])

            try:
                with open(snapshot1_path, 'r') as f:
                    data1 = json.load(f)
                with open(snapshot2_path, 'r') as f:
                    data2 = json.load(f)
            except Exception as e:
                self.failed(f"Error while loading snapshots for {device_name}: {str(e)}")
                continue

            device_differences_found = False

            for interface in data1:
                old_counters = data1[interface].get("counters", {}).get("netstat", {})
                new_counters = data2[interface].get("counters", {}).get("netstat", {})

                old_rx_drop = old_counters.get("rx-drop", 0)
                new_rx_drop = new_counters.get("rx-drop", 0)
                old_rx_error = old_counters.get("rx-error", 0)
                new_rx_error = new_counters.get("rx-error", 0)
                old_tx_drop = old_counters.get("tx-drop", 0)
                new_tx_drop = new_counters.get("tx-drop", 0)
                old_tx_error = old_counters.get("tx-error", 0)
                new_tx_error = new_counters.get("tx-error", 0)

                rx_drop_percent = self.calculate_percentage_change(old_rx_drop, new_rx_drop)
                rx_error_percent = self.calculate_percentage_change(old_rx_error, new_rx_error)
                tx_drop_percent = self.calculate_percentage_change(old_tx_drop, new_tx_drop)
                tx_error_percent = self.calculate_percentage_change(old_tx_error, new_tx_error)

                status = "Pass" if (old_rx_drop == new_rx_drop and 
                                    old_rx_error == new_rx_error and 
                                    old_tx_drop == new_tx_drop and 
                                    old_tx_error == new_tx_error) else "Fail"

                if status == "Fail":
                    device_differences_found = True

                all_data.append([device_name, interface, 
                                old_rx_drop, new_rx_drop, rx_drop_percent,
                                old_rx_error, new_rx_error, rx_error_percent,
                                old_tx_drop, new_tx_drop, tx_drop_percent,
                                old_tx_error, new_tx_error, tx_error_percent,
                                status])

            if device_differences_found:
                console.print(f"Differences found in interface counters for {device_name}.")
                differences_found = True

        # Convert data to DataFrame
        df = pd.DataFrame(all_data, columns=[
            "Device", "Interface", "Old rx-drop", "New rx-drop", "rx-drop %",
            "Old rx-error", "New rx-error", "rx-error %", "Old tx-drop", "New tx-drop", "tx-drop %",
            "Old tx-error", "New tx-error", "tx-error %", "Status"
        ])
        
        # Save to Excel
        wb = Workbook()
        ws = wb.active
        ws.title = "Interface Counters Comparison"
        
        row_offsets = {}  # Dictionary to store the start row of each device
        for r in dataframe_to_rows(df, index=False, header=True):
            ws.append(r)
            if r[0] not in row_offsets:
                row_offsets[r[0]] = ws.max_row  # Store the first occurrence row of each device
        
        # Add combined TX/RX charts to the Excel file
        self.add_combined_charts_to_excel(ws, df, row_offsets)

        wb.save(EXCEL_REPORT_PATH)
        
        if differences_found:
            self.failed("Differences found in interface counters for one or more devices.")
        else:
            self.passed("No differences found in interface counters for all devices.")

    def calculate_percentage_change(self, old_value, new_value):
        if old_value == 0:
            return 100 if new_value > 0 else 0
        try:
            return round(((new_value - old_value) / old_value) * 100, 2)
        except ZeroDivisionError:
            return 0

    def add_combined_charts_to_excel(self, ws, df, row_offsets):
        devices = df['Device'].unique()

        for device in devices:
            device_data = df[df['Device'] == device]
            start_row = row_offsets[device]  # Get the start row for the device data

            # Create the combined bar chart
            chart = BarChart()
            chart.title = f"{device} TX/RX Drop Differences (%)"
            chart.y_axis.title = 'Percentage'
            chart.x_axis.title = 'Interface'

            # Use the current table's data for the chart
            categories = Reference(ws, min_col=2, min_row=start_row, max_row=start_row + len(device_data) - 1)
            rx_data = Reference(ws, min_col=5, min_row=start_row, max_row=start_row + len(device_data) - 1)
            tx_data = Reference(ws, min_col=11, min_row=start_row, max_row=start_row + len(device_data) - 1)

            rx_series = Series(rx_data, title='RX Drop %')
            tx_series = Series(tx_data, title='TX Drop %')

            # Set colors
            rx_series.graphicalProperties.solidFill = "FFA500"  # Orange
            tx_series.graphicalProperties.solidFill = "0000FF"  # Blue

            chart.append(rx_series)
            chart.append(tx_series)
            chart.set_categories(categories)

            # Position chart after the last column (column 15, after "Status")
            chart_position = f"P{start_row}"
            ws.add_chart(chart, chart_position)

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
