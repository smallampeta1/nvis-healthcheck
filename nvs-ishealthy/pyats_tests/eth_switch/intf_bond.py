from rich.console import Console
from rich.table import Table
import pandas as pd
import json
import logging
from pyats import aetest
from pyats.async_ import pcall
from excel_functions.port_report import create_port_report

logger = logging.getLogger(__name__)

def connect_and_execute(device_info, command):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute(command)
        
        return {'status': 'success', 'device': device.name, 'output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class BondInterfaceCheck(aetest.Testcase):
    @aetest.test
    def verify_bond_status(self, device_infos, steps):
        command = 'net show interface bonds json'
        commands = [{'device_info': di, 'command': command} for di in device_infos]
        responses = pcall(connect_and_execute, ikwargs=commands)

        all_results = []
        for result in responses:
            device_name = result['device']
            results = []
            with steps.start(f"Verify Bond Interface Status on {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    results.append([device_name, '-', '-', '-', '-', '-', 'Device is not reachable'])
                    step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    continue

                try:
                    parsed_output = json.loads(result['output'])
                    if not parsed_output:
                        step.failed(f"No bond interface data found for {device_name}.")
                        continue
                except json.JSONDecodeError:
                    step.failed(f"No bond interface data found for {device_name}.")
                    continue

                for bond_name, bond_data in parsed_output.items():
                    bond_status = bond_data.get('linkstate', 'UNKNOWN')
                    bond_speed = bond_data.get('speed', 'N/A')
                    bond_mode = bond_data.get('mode', 'N/A')
                    bond_members = bond_data.get('summary', 'N/A')
                    pass_fail = "PASS" if bond_status == "UP" else "FAIL"
                    results.append([device_name, bond_name, bond_status, bond_speed, bond_mode, bond_members, pass_fail])

                # Print results in a table using Rich
                console = Console()
                table = Table(show_header=True, title=f"{device_name} Bond Interface Status", header_style="bold magenta")
                table.add_column("Device")
                table.add_column("Bond Name")
                table.add_column("Status")
                table.add_column("Speed")
                table.add_column("Mode")
                table.add_column("Bond Members")
                table.add_column("Pass/Fail")

                for row in results:
                    row_style = "green" if row[6] == "PASS" else "red"
                    table.add_row(*[str(cell) for cell in row], style=row_style)

                if results:
                    console.print("\n")
                    console.print(table, justify='left')

                all_results.extend(results)
                
                # Update the step result based on the presence of any 'FAIL' statuses
                if any(row[6] == "FAIL" for row in results):
                    step.failed(f"There are bond interface issues on {device_name}.")
                else:
                    step.passed(f"All bond interfaces on {device_name} are OK.")

        # Store results in the dataframes dictionary
        self.parent.parameters.setdefault('dataframes', {})['bond_intf'] = pd.DataFrame(all_results, columns=[
            "Device", "Bond Name", "Status", "Speed", "Mode", "Bond Members", "Pass/Fail"
        ])

    @aetest.cleanup
    def create_bond_report(self):
        dataframes = self.parent.parameters['dataframes']
        report_file_path = './pyats_reports/eth_switch/intf_bond_report.xlsx'
        create_port_report(dataframes, report_file_path, report_type="intf_check")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
