from rich.console import Console
from rich.table import Table
from rich.text import Text
import pandas as pd
import textfsm
import os
import logging
from pyats import aetest
from pyats.async_ import pcall
from excel_functions.port_report import *
from datetime import datetime

# Configure logging
logger = logging.getLogger(__name__)

def connect_and_execute_lldp(device_info):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        # Execute the command
        output = device.execute("net show lldp")
        return {'status': 'success', 'device': device.name, 'output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class LLDPNeighborCheck(aetest.Testcase):
    @aetest.test
    def verify_lldp_neighbors(self, steps, device_infos):
        template_path = './templates/eth_switch/net_sh_lldp.template'
        portmap_csv_path = './p2p/eth_p2p.csv'
        
        assert os.path.exists(template_path), f"Template file does not exist: {template_path}"
        assert os.path.exists(portmap_csv_path), f"Portmap CSV file does not exist: {portmap_csv_path}"

        portmap_df = pd.read_csv(portmap_csv_path, names=[
            'LocalRack', 'LocalRU', 'LocalHost', 'LocalPort', 'RemoteRack', 'RemoteRU', 'RemoteHost_expected', 'RemotePort_expected'
        ], header=0, dtype=str)

        console = Console()
        results = []

        # Use ikwargs to pass each DeviceInfo object to pcall
        parallel_results = pcall(connect_and_execute_lldp, ikwargs=[{'device_info': di} for di in device_infos])

        for result in parallel_results:
            device_name = result['device']
            with steps.start(f"Checking LLDP neighbors for {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    logger.error(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    results.append([device_name, '-', '-', '-', '-', '-', '-', '-', '-', '-', 'Device is not reachable', ''])
                    step.failed(f"Failed to connect or execute command on {device_name}")
                    continue

                output = result['output']
                if not output.strip() or "No interfaces found for LLDP." in output:
                    results.append([device_name, '-', '-', '-', '-', '-', '-', '-', '-', '-', 'No LLDP data found.', ''])
                    step.failed(f"No LLDP data found for {device_name}. It might be due to LLDP being disabled.")
                    continue

                with open(template_path) as template_file:
                    fsm = textfsm.TextFSM(template_file)
                    lldp_data = fsm.ParseText(output)

                    if not lldp_data:
                        step.failed(f"No LLDP data found for {device_name}.")
                        continue

                lldp_data_with_host = [[device_name] + row for row in lldp_data if row[0] not in ('LocalPort', '---------')]
                lldp_df = pd.DataFrame(lldp_data_with_host, columns=['LocalHost', 'LocalPort', 'RemoteHost_current', 'RemotePort_current'])

                # Create a combined portmap DataFrame where both a<->z and z<->a are considered
                portmap_combined = portmap_df.copy()
                reversed_entries = portmap_df.copy()

                # Swap columns for reversed entries
                reversed_entries.columns = [
                    'RemoteRack', 'RemoteRU', 'RemoteHost_expected', 'RemotePort_expected', 'LocalRack', 'LocalRU', 'LocalHost', 'LocalPort'
                ]
                portmap_combined = pd.concat([portmap_combined, reversed_entries], ignore_index=True)

                comparison_df = pd.merge(lldp_df, portmap_combined, how='outer', on=['LocalHost', 'LocalPort'], suffixes=('_current', '_expected'))
                comparison_df.fillna('-', inplace=True)

                comparison_df['Status'] = comparison_df.apply(
                    lambda x: 'PASS' if (x['RemoteHost_current'] == x['RemoteHost_expected'] and x['RemotePort_current'] == x['RemotePort_expected']) else 'FAIL', axis=1)

                # Add Comments column
                comparison_df['Comments'] = comparison_df.apply(
                    lambda x: 'Cable not connected/down' if x['RemoteHost_current'] == '-' and x['RemotePort_current'] == '-' else
                              'Miss Cabled' if x['Status'] == 'FAIL' else '', axis=1)

                # Filter to only include rows where the device is the LocalHost
                filtered_comparison_df = comparison_df[comparison_df['LocalHost'] == device_name]

                for _, row in filtered_comparison_df.iterrows():
                    results.append([
                        row['LocalRack'], row['LocalRU'], row['LocalHost'], row['LocalPort'], row['RemoteRack'], row['RemoteRU'],
                        row['RemoteHost_expected'], row['RemotePort_expected'], row['RemoteHost_current'], row['RemotePort_current'],
                        row['Status'], row['Comments']
                    ])

                table = Table(show_header=True, header_style="bold magenta")
                table.add_column("LocalRack")
                table.add_column("LocalRU")
                table.add_column("LocalHost")
                table.add_column("LocalPort")
                table.add_column("RemoteRack")
                table.add_column("RemoteRU")
                table.add_column("Expected RemoteHost")
                table.add_column("Expected RemotePort")
                table.add_column("Current RemoteHost")
                table.add_column("Current RemotePort")
                table.add_column("Status", justify="center")
                table.add_column("Comments", justify="center")

                for _, row in filtered_comparison_df.iterrows():
                    status_style = "bold red" if row['Status'] == 'FAIL' else "green"
                    local_style = "bold yellow" if row['Status'] == 'FAIL' else "white"
                    expected_style = "bold green" if row['Status'] == 'FAIL' else ""
                    current_style = "bold red" if row['Status'] == 'FAIL' else ""

                    table.add_row(
                        Text(str(row['LocalRack']), style=local_style),
                        Text(str(row['LocalRU']), style=local_style),
                        Text(str(row['LocalHost']), style=local_style),
                        Text(str(row['LocalPort']), style=local_style),
                        Text(str(row['RemoteRack']), style=expected_style),
                        Text(str(row['RemoteRU']), style=expected_style),
                        Text(str(row.get('RemoteHost_expected', '-')), style=expected_style) if row.get('RemoteHost_expected', '-') != 'nan' else Text('-', style=expected_style),
                        Text(str(row.get('RemotePort_expected', '-')), style=expected_style) if row.get('RemotePort_expected', '-') != 'nan' else Text('-', style=expected_style),
                        Text(str(row.get('RemoteHost_current', '-')), style=current_style) if row.get('RemoteHost_current', '-') != 'nan' else Text('-', style=current_style),
                        Text(str(row.get('RemotePort_current', '-')), style=current_style) if row.get('RemotePort_current', '-') != 'nan' else Text('-', style=current_style),
                        Text(str(row['Status']), style=status_style),
                        Text(str(row['Comments']), style=status_style) if row['Status'] == 'FAIL' else Text('')
                    )

                console.print(f"LLDP data for {device_name}:")
                console.print(table)

                if any(filtered_comparison_df['Status'] == 'FAIL'):
                    step.failed("There are mismatches in LLDP data for " + device_name + ".")
                else:
                    step.passed("All LLDP data matches expected values for " + device_name + ".")

        # Store results in the parameters dictionary
        self.parent.parameters['dataframes'] = {'cable_check': pd.DataFrame(results, columns=[
            'LocalRack', 'LocalRU', 'LocalHost', 'LocalPort', 'RemoteRack', 'RemoteRU', 'Expected RemoteHost',
            'Expected RemotePort', 'Current RemoteHost', 'Current RemotePort', 'Status', 'Comments'])}

    @aetest.cleanup
    def create_port_report(self):
        dataframes = self.parent.parameters.get('dataframes', {})
        if 'cable_check' not in dataframes:
            self.failed("No data available for report generation.")

        # Generate a timestamp for the report file name
        timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        report_file_path = f'./pyats_reports/eth_switch/cable_checker/cable_checker_{timestamp}.xlsx'
        create_port_report(dataframes, report_file_path, report_type="port_checker")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()