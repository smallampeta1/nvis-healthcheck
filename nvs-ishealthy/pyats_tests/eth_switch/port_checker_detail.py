from rich.console import Console
from rich.table import Table
from rich.text import Text
import json
import pandas as pd
import textfsm
import os
import csv
import logging
from pyats import aetest

logger = logging.getLogger(__name__)


class CommonSetup(aetest.CommonSetup):
    @aetest.subsection
    def connect_to_devices(self, steps, filtered_devices,testbed, p_connect=False):
        """
        Establishes connections to all the filtered testbed devices.
        To allow the test to continue, all the devices need to be UP.
        """
        if p_connect:
            try:
                # Use p_connect=True for parallel connections, specifying the devices to connect
                testbed.connect(p_connect=True, device_names=filtered_devices, log_stdout=False)
                logger.info("Successfully connected to all devices in parallel.")
            except Exception as e:
                logger.error(f"Failed to establish parallel connections due to: {e}")
        else:
            for device_name, device in filtered_devices.items():
                try:
                    device.connect(log_stdout=False)
                except Exception as e:
                    logger.info(f"Failed to establish connection to {device} due to: {e}")

class LLDPNeighborCheck(aetest.Testcase):

    @aetest.test
    def verify_lldp_neighbors(self, filtered_devices, steps):
        portmap_csv_path = './p2p/eth_p2p.csv'
        report_csv_path = './pyats_reports/eth_switch/port_checker_detail.csv'

        assert os.path.exists(portmap_csv_path), f"Portmap CSV file does not exist: {portmap_csv_path}"

        portmap_df_all = pd.read_csv(portmap_csv_path)

        console = Console()

        with open(report_csv_path, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['LocalHost', 'LocalPort', 'Expected RemoteHost', 'Expected RemotePort', 'Current RemoteHost', 'Current RemotePort','Link Speed', 'Link State','In-Errors','Out-Errors','SFP Type','SFP Vendor-Name','SFP Vendor-PN','Status'])

            for device_name, device in filtered_devices.items():
                with steps.start(f"Checking LLDP neighbors for {device_name}", continue_=True) as step:
                    if device.os.lower() != 'cumulus':
                        step.failed(f"{device_name} OS is not 'cumulus'. Skipping LLDP check.")
                        continue
                    if not device.connected:
                        csvwriter.writerow([device_name, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'Device is not reachable'])
                        step.failed(f"{device_name} is not connected. Skipping port checker test.")
                        continue

                    # Filter portmap DataFrame for the current device
                    portmap_df = portmap_df_all[portmap_df_all['LocalHost'] == device_name]

                    if portmap_df.empty:
                        # Log message to CSV and console if portmap information is missing
                        csvwriter.writerow([device_name, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'No portmap information'])
                        step.failed(f"No portmap information found for {device_name}.")
                        continue

                    results = []

                    for _, port_info in portmap_df.iterrows():
                        local_port = port_info['LocalPort']
                        command = f"nv show interface {local_port} -o json"
                        try:
                            output = device.execute(command)
                            if not output.strip():
                                step.failed(f"No output received from {device_name} for port {local_port}.")
                                continue
                            interface_data = json.loads(output)
                            

                            # Extracting LLDP Neighbor information correctly
                            neighbors = interface_data.get("lldp", {}).get("neighbor", {})
                            if neighbors:
                                # Extracting the first neighbor's information for simplicity
                                first_neighbor_key = next(iter(neighbors.keys()))
                                first_neighbor = neighbors[first_neighbor_key]
                                current_remote_host = first_neighbor.get("chassis", {}).get("system-name", "N/A")
                                current_remote_port = first_neighbor.get("port", {}).get("name", "N/A")
                            else:
                                current_remote_host = "N/A"
                                current_remote_port = "N/A"

                            link_speed = interface_data.get("link", {}).get("speed", "N/A")
                            link_state = "up" if "up" in interface_data.get("link", {}).get("state", {}) else "down"
                            in_errors = str(interface_data.get("link", {}).get("stats", {}).get("in-errors", "N/A"))
                            out_errors = str(interface_data.get("link", {}).get("stats", {}).get("out-errors", "N/A"))
                            sfp_id = interface_data.get("pluggable", {}).get("identifier", "N/A")
                            sfp_vendor_name = interface_data.get("pluggable", {}).get("vendor-name", "N/A")
                            sfp_vendor_pn = interface_data.get("pluggable", {}).get("vendor-pn", "N/A")

                            # Preparing result for comparison and table display
                            results.append([
                                device_name,
                                local_port,
                                port_info['RemoteHost'],
                                port_info['RemotePort'],
                                current_remote_host,
                                current_remote_port,
                                link_speed,
                                link_state,
                                in_errors,
                                out_errors,
                                sfp_id,
                                sfp_vendor_name,
                                sfp_vendor_pn,
                                "PASS" if (port_info['RemoteHost'] == current_remote_host and port_info['RemotePort'] == current_remote_port) else "FAIL"
                            ])
                        except Exception as e:
                            step.failed(f"Could not execute command on {device_name}: {e}")
                            continue
                    # Create a DataFrame for comparison results
                    comparison_df = pd.DataFrame(results, columns = ["LocalHost", "LocalPort", "Expected RemoteHost", "Expected RemotePort",
                    "Current RemoteHost", "Current RemotePort", "Link Speed", "Link State",
                    "In-Errors", "Out-Errors", "SFP Type", "SFP Vendor-Name", "SFP Vendor-PN", "Status"])

                    # Add a 'Status' column based on the comparison
                    comparison_df['Status'] = comparison_df.apply(lambda row: 'PASS' if (
                        row['Expected RemoteHost'] == row['Current RemoteHost'] and
                        row['Expected RemotePort'] == row['Current RemotePort']) else 'FAIL', axis=1)

                    # Generate and display the table
                    table = Table(show_header=True, title=f"LLDP data for {device_name}", header_style="bold magenta")
                    for col in comparison_df.columns:
                        table.add_column(col)

                    for _, row in comparison_df.iterrows():
                        status_style = "bold red" if row['Status'] == 'FAIL' else "green"
                        local_style = "bold yellow" if row['Status'] == 'FAIL' else "white"

                        expected_style = "" if row['Status'] == 'PASS' else "bold green"
                        current_style = "" if row['Status'] == 'PASS' else "bold red"
                        white_style = "white"

                        csvwriter.writerow([row['LocalHost'], row['LocalPort'], row['Expected RemoteHost'], row['Expected RemotePort'], row['Current RemoteHost'], row['Current RemotePort'],row['Link Speed'], row['Link State'], row['In-Errors'], row['Out-Errors'],row['SFP Type'],row['SFP Vendor-Name'],row['SFP Vendor-PN'], row['Status']])
                        # Convert each value to a Text object with appropriate style
                        table.add_row(
                            Text(str(row['LocalHost']), style=local_style),
                            Text(str(row['LocalPort']), style=local_style),
                            Text(str(row.get('Expected RemoteHost', '-')), style=expected_style),
                            Text(str(row.get('Expected RemotePort', '-')), style=expected_style),
                            Text(str(row.get('Current RemoteHost', '-')), style=current_style),
                            Text(str(row.get('Current RemotePort', '-')), style=current_style),
                            Text(str(row['Link Speed']), style=white_style),
                            Text(str(row['Link State']), style=white_style),
                            Text(str(row['In-Errors']), style=white_style),
                            Text(str(row['Out-Errors']), style=white_style),
                            Text(str(row['SFP Type']), style=white_style),
                            Text(str(row['SFP Vendor-Name']), style=white_style),
                            Text(str(row['SFP Vendor-PN']), style=white_style),
                            Text(row['Status'], style=status_style)
                        )
                    console.print(table)

                    # Update the step result based on the presence of any 'FAIL' statuses
                    if 'FAIL' in comparison_df['Status'].values:
                        step.failed(f"There are mismatches in LLDP data for {device_name}.")
                    else:
                            step.passed(f"All LLDP data matches expected values for {device_name}.")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")


if __name__ == '__main__':
    aetest.main()
