from pyats import aetest
from rich.console import Console
from rich.table import Table
import textfsm
import pandas as pd
import os
import logging
from pyats.async_ import pcall
from datetime import datetime
from excel_functions.underlay_report import create_bgp_report

# Configure logging
logger = logging.getLogger(__name__)

def connect_and_execute_bgp(device_info):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute("net show bgp summary")
        
        # Parse the output using textfsm
        template_dir = './templates/'
        template_file = os.path.join(template_dir, 'eth_switch/net_sh_bgp_summary.template')
        with open(template_file) as template:
            fsm = textfsm.TextFSM(template)
            parsed_output = fsm.ParseText(output)
        
        return {'status': 'success', 'device': device.name, 'output': parsed_output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class BGPCheck(aetest.Testcase):
    @aetest.test
    def verify_bgp_status(self, steps, device_infos):
        """
        Verifies BGP status on all connected devices and outputs results.
        """
        results = []

        # Use ikwargs to pass each DeviceInfo object to pcall
        responses = pcall(connect_and_execute_bgp, ikwargs=[{'device_info': di} for di in device_infos])
        
        for result in responses:
            device_name = result['device']
            with steps.start(f"Verify BGP Status on {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    results.append([device_name, "No BGP Neighbors found", "", "", "FAIL"])
                    step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    continue

                parsed_output = result['output']
                if not parsed_output:
                    results.append([device_name, "No BGP Neighbors found", "", "", "FAIL"])
                    logger.error(f"No BGP neighbors found on {device_name}.")
                    step.failed(f"No BGP neighbors found on {device_name}")
                    continue

                console = Console()
                table = Table(show_header=True, title=f"{device_name} BGP Status", header_style="bold magenta")
                table.add_column("Device")
                table.add_column("BGP Neighbor")
                table.add_column("BGP ASN")
                table.add_column("BGP State")
                table.add_column("Pass/Fail")
                test_failed = False

                for row in parsed_output:
                    item_name = row[0]
                    item_asn = row[1]
                    item_state = row[2]
                    pass_fail = "PASS" if item_state.lower() != "never" else "FAIL"
                    row_style = "green" if pass_fail == "PASS" else "red"

                    if pass_fail == "FAIL":
                        test_failed = True

                    results.append([device_name, item_name, item_asn, item_state, pass_fail])
                    table.add_row(device_name, item_name, item_asn, item_state, pass_fail, style=row_style)

                console.print("\n")
                console.print(table, justify='left')

                if test_failed:
                    step.failed(f"BGP check failed on {device_name}. See table above for details.")
                else:
                    step.passed(f"All BGPs on {device_name} are OK")

        # Store results in the dataframes dictionary
        self.parent.parameters.setdefault('dataframes', {})['bgp_status'] = pd.DataFrame(results, columns=[
            "Device", "BGP Neighbor", "BGP ASN", "BGP State", "Pass/Fail"
        ])

    @aetest.cleanup
    def create_bgp_report(self):
        dataframes = self.parent.parameters['dataframes']
        report_file_path = './pyats_reports/eth_switch/bgp_status_report.xlsx'
        create_bgp_report(dataframes, report_file_path, report_type="bgp_check")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
