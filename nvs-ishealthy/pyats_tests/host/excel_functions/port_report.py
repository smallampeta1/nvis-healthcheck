import pandas as pd
import datetime

def calculate_major_unit(max_value):
    """Calculate an appropriate major unit for the y-axis based on the maximum value."""
    if max_value <= 10:
        return 1
    elif max_value <= 100:
        return 10
    elif max_value <= 1000:
        return 100
    else:
        return 500

def create_bar_chart(writer, worksheet, df, title, start_row, start_col, status_column_index, report_date_time):
    # Use the specified column for status
    status_column = df.columns[status_column_index]
    # Filter only Pass and Fail statuses
    filtered_df = df[df[status_column].isin(['PASS', 'FAIL'])]
    total_items = len(filtered_df)
    state_counts = filtered_df[status_column].value_counts()

    # Include count of non-reachable devices separately
    non_reachable_count = len(df[df[status_column] == 'Device is not reachable'])

    # Combine total items and state counts into a new DataFrame
    categories = ['Total'] + state_counts.index.tolist() + ['Device\nNon-Reachable']
    counts = [total_items] + state_counts.tolist() + [non_reachable_count]
    summary_df = pd.DataFrame({'Category': categories, 'Count': counts})

    # Write summary DataFrame to worksheet
    summary_start_row = start_row + len(df) + 2  # Adjusted row position for summary
    summary_df.to_excel(writer, sheet_name=worksheet.get_name(), startrow=summary_start_row, startcol=start_col, index=False)

    # Create bar chart
    chart = writer.book.add_chart({'type': 'column'})
    points = [{'fill': {'color': '#0000FF'}},  # Blue for Total
              {'fill': {'color': '#006400'}},  # Dark Green for Pass
              {'fill': {'color': '#FFA500'}},  # Orange for Fail
              {'fill': {'color': '#FF0000'}}]  # Red for Device Non-Reachable

    chart.add_series({
        'name': title,
        'categories': [worksheet.get_name(), summary_start_row + 1, start_col, summary_start_row + len(summary_df), start_col],
        'values': [worksheet.get_name(), summary_start_row + 1, start_col + 1, summary_start_row + len(summary_df), start_col + 1],
        'points': points[:len(summary_df)],
        'data_labels': {'value': True}  # Show value labels on top of each bar
    })
    chart.set_title({'name': f'{title} (Report Date: {report_date_time})'})
    chart.set_x_axis({'name': 'Category'})
    chart.set_y_axis({
        'name': 'Count',
        'major_gridlines': {'visible': False},
        'major_unit': calculate_major_unit(max(summary_df['Count']))
    })

    # Set the size of the chart
    chart.set_size({'width': 800, 'height': 500})

    # Insert the chart into the worksheet a couple of columns away from the last column and a couple of rows down from the top
    chart_start_col = len(df.columns) + 2
    chart_start_row = 2
    worksheet.insert_chart(chart_start_row, chart_start_col, chart)

def create_port_report(dataframes, report_file_path, report_type=None):
    report_date_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    with pd.ExcelWriter(report_file_path, engine='xlsxwriter') as writer:
        for check_type, df in dataframes.items():
            sheet_name = check_type.replace('_', ' ').capitalize()  # Convert keys to a readable format for sheet names
            start_row = 0  # Adjusted starting row for writing the DataFrame
            df.to_excel(writer, sheet_name=sheet_name, startrow=start_row, index=False)
            worksheet = writer.sheets[sheet_name]

            # Apply conditional formatting to the headers
            header_format_yellow = writer.book.add_format({'bold': True, 'bg_color': '#FFFF00'})
            header_format_red = writer.book.add_format({'bold': True, 'bg_color': '#FF0000'})
            header_format_orange = writer.book.add_format({'bold': True, 'bg_color': '#FFA500'})
            header_format_default = writer.book.add_format({'bold': True})

            for col_num, column in enumerate(df.columns):
                if column.startswith('Local'):
                    worksheet.write(start_row, col_num, column, header_format_yellow)
                elif column.startswith('Remote'):
                    worksheet.write(start_row, col_num, column, header_format_red)
                elif column.startswith('Expected'):
                    worksheet.write(start_row, col_num, column, header_format_red)
                elif column.startswith('Current'):
                    worksheet.write(start_row, col_num, column, header_format_orange)
                else:
                    worksheet.write(start_row, col_num, column, header_format_default)

            # Adjust column widths
            for i, column in enumerate(df.columns):
                column_width = max(df[column].astype(str).map(len).max(), len(column)) + 2
                worksheet.set_column(i, i, column_width)

            if check_type == "cable_check":
                create_bar_chart(writer, worksheet, df, "Cable Check Summary", start_row, 0, -2, report_date_time)
            elif check_type == "bond_intf":
                create_bar_chart(writer, worksheet, df, "Bond Interface Check Summary", start_row, 0, -1, report_date_time)
            elif check_type == "ping_check":
                create_bar_chart(writer, worksheet, df, "Ping Check Summary", start_row, 0, -1, report_date_time)
