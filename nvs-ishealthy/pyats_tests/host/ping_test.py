from rich.console import Console
from rich.table import Table
from rich.text import Text
import pandas as pd
import json
import os
import logging
from pyats import aetest
from pyats.async_ import pcall
from excel_functions.port_report import *
from datetime import datetime

# Configure logging
logger = logging.getLogger(__name__)

def connect_execute_and_ping(device_info):
    device = device_info.device
    results = []
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        # Execute the ifconfig command
        output = device.execute("ifconfig")
        
        # Parse interfaces and IP addresses
        interfaces = {}
        current_interface = None
        for line in output.splitlines():
            if 'eth_rail' in line:
                current_interface = line.split()[0]
            if current_interface and 'inet ' in line:
                inet_ip = line.split()[1]
                interfaces[current_interface] = inet_ip
                current_interface = None
        
        # Perform fping for all remote IPs at once
        remote_ips = [ping_remote_ip(ip) for ip in interfaces.values()]
        fping_results = device.execute(f"fping -c 1 -t 100 {' '.join(remote_ips)}")

        # Parse fping results
        for interface, local_ip in interfaces.items():
            remote_ip = ping_remote_ip(local_ip)
            # Check if the remote IP has a successful ping result
            remote_ip_found = False
            for line in fping_results.splitlines():
                if line.startswith(remote_ip):
                    remote_ip_found = True
                    if "0% loss" in line:
                        status = "PASS"
                    else:
                        status = "FAIL"
                    break
            if not remote_ip_found:
                status = "FAIL"
            results.append([device.name, interface, local_ip, remote_ip, status])
        
        return {'status': 'success', 'device': device.name, 'results': results}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

def ping_remote_ip(local_ip):
    octets = local_ip.split('.')
    last_octet = int(octets[-1])
    if last_octet % 2 == 0:
        remote_ip = f"{octets[0]}.{octets[1]}.{octets[2]}.{last_octet + 1}"
    else:
        remote_ip = f"{octets[0]}.{octets[1]}.{octets[2]}.{last_octet - 1}"
    return remote_ip

class PingTest(aetest.Testcase):
    @aetest.test
    def verify_ping(self, steps, device_infos):
        console = Console()
        all_results = []

        # Use ikwargs to pass each DeviceInfo object to pcall
        parallel_results = pcall(connect_execute_and_ping, ikwargs=[{'device_info': di} for di in device_infos])

        for result in parallel_results:
            device_name = result['device']
            with steps.start(f"Pinging remote IPs for {device_name}", continue_=True) as step:
                if result['status'] == 'failed':
                    logger.error(f"Failed to connect or execute command on {device_name}: {result['error']}")
                    all_results.append([device_name, '-', '-', '-', 'Device is not reachable'])
                    step.failed(f"Failed to connect or execute command on {device_name}")
                    continue

                device_results = result['results']
                all_results.extend(device_results)

                table = Table(show_header=True, header_style="bold magenta")
                table.add_column("Device Name")
                table.add_column("Interface Name")
                table.add_column("Local IP Address")
                table.add_column("Remote IP Address")
                table.add_column("Status", justify="center")

                for row in device_results:
                    status_style = "bold red" if row[4] == 'FAIL' else "green"
                    table.add_row(
                        Text(row[0], "white"),
                        Text(row[1], "white"),
                        Text(row[2], "white"),
                        Text(row[3], "white"),
                        Text(row[4], status_style)
                    )

                console.print(f"Ping results for {device_name}:")
                console.print(table)

                if any(row[4] == 'FAIL' for row in device_results):
                    step.failed(f"Some pings failed for {device_name}.")
                else:
                    step.passed(f"All pings successful for {device_name}.")

        # Store results in the parameters dictionary
        self.parent.parameters['dataframes'] = {'ping_check': pd.DataFrame(all_results, columns=[
            'Device Name', 'Interface Name', 'Local IP Address', 'Remote IP Address', 'Status'])}

    @aetest.cleanup
    def create_ping_report(self):
        dataframes = self.parent.parameters.get('dataframes', {})
        if 'ping_check' not in dataframes:
            self.failed("No data available for report generation.")

        # Generate a timestamp for the report file name
        timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        report_file_path = f'./pyats_reports/host/ping_report/ping_report_{timestamp}.xlsx'
        create_port_report(dataframes, report_file_path, report_type="ping_check")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
