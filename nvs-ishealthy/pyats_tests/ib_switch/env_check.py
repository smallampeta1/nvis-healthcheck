from rich.console import Console
from rich.table import Table
import textfsm
import csv
import os
import logging
from pyats import aetest
from pyats.async_ import pcall

logger = logging.getLogger(__name__)

def connect_and_execute(device_info, command, template_file=None):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute(command)
        
        # Parse the output using textfsm if template_file is provided
        parsed_output = None
        if template_file:
            with open(template_file) as template:
                fsm = textfsm.TextFSM(template)
                parsed_output = fsm.ParseText(output)
        
        return {'status': 'success', 'device': device.name, 'output': parsed_output, 'raw_output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}


class DeviceHealthCheck(aetest.Testcase):
    def _verify_status(self, device_infos, steps, check_type):
        template_dir = './templates/'
        check_data = {
            'psu': {
                'command': 'show power',
                'template': 'ib_switch/sh_power.template',
                'report_file_name': 'psu_status_report'
            },
            'fan': {
                'command': 'show fan',
                'template': 'ib_switch/sh_fan.template',
                'report_file_name': 'fan_status_report'
            },
            'led': {
                'command': 'show leds',
                'template': 'ib_switch/sh_leds.template',
                'report_file_name': 'led_status_report'
            },
            'voltage': {
                'command': 'show voltage',
                'template': 'ib_switch/sh_voltage.template',
                'report_file_name': 'voltage_status_report'
            },
        }

        check_info = check_data.get(check_type)
        if not check_info:
            return

        commands = [{'device_info': di, 'command': check_info['command'], 'template_file': os.path.join(template_dir, check_info['template'])} for di in device_infos]
        responses = pcall(connect_and_execute, ikwargs=commands)

        csv_file_path = f'./pyats_reports/ib_switch/{check_info["report_file_name"]}.csv'

        # Adjust CSV and table headers based on check_type
        if check_type == 'voltage':
            csv_headers = ["Device", "Module Name", "Power Meter", "Voltage State", "Pass/Fail"]
            table_columns = ["Device", "Module Name", "Power Meter", "Voltage State", "Pass/Fail"]
        else:
            csv_headers = ["Device", f"{check_type.capitalize()} Name", f"{check_type.capitalize()} State", "Pass/Fail"]
            table_columns = ["Device", f"{check_type.capitalize()} Name", f"{check_type.capitalize()} State", "Pass/Fail"]

        with open(csv_file_path, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(csv_headers)

            for result in responses:
                device_name = result['device']
                with steps.start(f"Verify {check_type.capitalize()} Status on {device_name}", continue_=True) as step:
                    if result['status'] == 'failed':
                        csvwriter.writerow([device_name, '-', '-', 'Device is not reachable'])
                        step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                        continue

                    parsed_output = result['output']
                    if not parsed_output:
                        csvwriter.writerow([device_name, f"No {check_type.upper()}'s found", "FAIL"])
                        step.failed(f"No {check_type}s found on {device_name}. Unable to verify {check_type} status.")
                        continue

                    console = Console()
                    table = Table(show_header=True, title=f"{device_name} {check_type.capitalize()} Status", header_style="bold magenta")
                    for col_name in table_columns:
                        table.add_column(col_name)
                    test_failed = False

                    for row in parsed_output:
                        if check_type == "led":
                            item_name, item_color = row
                            pass_fail = "PASS" if item_color.lower() == "green" else "FAIL"
                            csv_row = [device_name, item_name, item_color, pass_fail]
                            table_row = [device_name, item_name, item_color, pass_fail]
                        elif check_type == "voltage":
                            item_name = row[0]  
                            power_meter = row[1]
                            item_state = row[5]
                            pass_fail = "PASS" if item_state.lower() == "ok" else "FAIL"
                            csv_row = [device_name, item_name, power_meter, item_state, pass_fail]
                            table_row = [device_name, item_name, power_meter, item_state, pass_fail]
                        else:  # For 'psu' and 'fan'
                            item_name = row[0]
                            item_state = row[-1]
                            pass_fail = "PASS" if item_state.lower() == 'ok' else "FAIL"
                            csv_row = [device_name, item_name, item_state, pass_fail]
                            table_row = [device_name, item_name, item_state, pass_fail]

                        row_style = "green" if pass_fail == "PASS" else "red"
                        
                        if pass_fail == "FAIL":
                            test_failed = True

                        csvwriter.writerow(csv_row)
                        table.add_row(*table_row, style=row_style) 

                    console.print("\n")
                    console.print(table, justify='left')

                    if test_failed:
                        step.failed(f"{check_type.capitalize()} check failed on {device_name}. See table above for details.")
                    else:
                        step.passed(f"All {check_type}s on {device_name} are OK")

    @aetest.test
    def verify_psu_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'psu')

    @aetest.test
    def verify_fan_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'fan')

    @aetest.test
    def verify_led_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'led')

    @aetest.test
    def verify_voltage_status(self, steps, device_infos):
        self._verify_status(device_infos, steps, 'voltage')

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
