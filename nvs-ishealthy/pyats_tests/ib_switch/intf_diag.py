from rich.console import Console
from rich.table import Table
import textfsm
import os
import csv
import logging
from pyats import aetest
from pyats.async_ import pcall

logger = logging.getLogger(__name__)

def connect_and_execute(device_info, command, template_file=None):
    device = device_info.device
    try:
        # Connect to the device
        device.connect(log_stdout=False)
        
        # Execute the command
        output = device.execute(command)
        
        # Parse the output using textfsm if template_file is provided
        parsed_output = None
        if template_file:
            with open(template_file) as template:
                fsm = textfsm.TextFSM(template)
                parsed_output = fsm.ParseText(output)
        
        return {'status': 'success', 'device': device.name, 'output': parsed_output, 'raw_output': output}
    except Exception as e:
        logger.error(f"Failed to connect or execute command on {device.name} due to: {e}")
        return {'status': 'failed', 'device': device.name, 'error': str(e)}

class InterfaceDiagnosticsCheck(aetest.Testcase):
    @aetest.test
    def verify_interface_status(self, steps, device_infos):
        template_dir = './templates/'
        commands = {
            'mellanox': 'show interfaces ib link-diagnostics',
        }
        templates = {
            'mellanox': 'ib_switch/sh_int_ib_link_diag.template',
        }

        csv_file_path = './pyats_reports/ib_switch/intf_diag_report.csv'
        
        # Ensure CSV file exists and write header if it's new
        with open(csv_file_path, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(["Device", "Interface", "Code", "Status", "Pass/Fail"])

            commands_list = [{'device_info': di, 'command': commands['mellanox'], 'template_file': os.path.join(template_dir, templates['mellanox'])} for di in device_infos]
            responses = pcall(connect_and_execute, ikwargs=commands_list)

            for result in responses:
                device_name = result['device']
                with steps.start(f"Verifying Interface Status on {device_name}", continue_=True) as step:
                    if result['status'] == 'failed':
                        csvwriter.writerow([device_name, '-', '-', 'Device is not reachable', 'FAIL'])
                        step.failed(f"Failed to connect or execute command on {device_name}: {result['error']}")
                        continue

                    parsed_output = result['output']
                    if not parsed_output:
                        csvwriter.writerow([device_name, '-', '-', 'No interface data found', 'FAIL'])
                        step.failed(f"No interface data found for {device_name}.")
                        continue

                    any_failures = False
                    console = Console()
                    table = Table(show_header=True, title=f"{device_name} Interface Status", header_style="bold magenta")
                    table.add_column("Device")
                    table.add_column("Interface")
                    table.add_column("Code")
                    table.add_column("Status")
                    table.add_column("Pass/Fail", justify="center")

                    for row in parsed_output:
                        interface_name, code, status = row[:3]
                        pass_fail = "PASS" if "no issue was observed" in status.lower() else "FAIL"
                        
                        if pass_fail == "FAIL":
                            any_failures = True
                        csvwriter.writerow([device_name, interface_name, code, status, pass_fail])
                        table.add_row(device_name, interface_name, code, status, pass_fail, style="red" if pass_fail == "FAIL" else "green")

                    console.print("\n")
                    console.print(table, justify='left')
                    
                    if any_failures:
                        step.failed(f"One or more interfaces on {device_name} have issues.")
                    else:
                        step.passed(f"All interfaces on {device_name} are OK")

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    aetest.main()
