import os
import time
from pyats.easypy import Task

def main(runtime):
    testbed_file = 'staging-testbed.yaml'
    snapshot_folder = 'snapshots'
    os.makedirs(snapshot_folder, exist_ok=True)
    
    # Generate a unique snapshot name using a timestamp
    timestamp = time.strftime("%Y%m%d_%H%M%S")
    new_snapshot = f'snapshot_{timestamp}.json'
    
    # Find the latest snapshot in the folder (if any)
    snapshots = sorted([f for f in os.listdir(snapshot_folder) if f.endswith('.json')])
    if snapshots:
        previous_snapshot = snapshots[-1]
    else:
        previous_snapshot = None
    
    # Task to take the new snapshot and compare with the previous one
    task = Task(
        testscript='interface_counters_test.py', 
        runtime=runtime, 
        taskid='take_and_compare_snapshots', 
        testbed_file=testbed_file, 
        snapshot_folder=snapshot_folder, 
        snapshot1=previous_snapshot, 
        snapshot2=new_snapshot
    )
    task.start()
    task.wait()  # Ensure task completes before proceeding
