import json
import os
from pyats import aetest
from pyats.topology import loader
from pyats.log.utils import banner
from rich.console import Console
import pandas as pd
from openpyxl import load_workbook
from openpyxl.chart import BarChart, Reference, Series
import logging

logger = logging.getLogger(__name__)

console = Console()

class InterfaceCountersTestcase(aetest.Testcase):
    @aetest.setup
    def setup(self, testbed_file):
        # Load the testbed
        self.testbed = loader.load(testbed_file)
        self.testbed.connect(log_stdout=False)

    def get_interface_counters(self, device):
        # Run the command to get interface counters in JSON format
        command = 'nv show interface counters -o json'
        try:
            output = device.execute(command)
            if not output.strip():
                self.failed(f"No output received from {device.name} when executing '{command}'")
            interface_data = json.loads(output)
            return interface_data
        except Exception as e:
            self.failed(f"Error while executing '{command}' on {device.name}: {str(e)}")

    @aetest.test
    def take_and_compare_snapshots(self, snapshot_folder='snapshots', snapshot1=None, snapshot2='snapshot2.json'):
        differences_found = False

        # Ensure the snapshot folder exists
        os.makedirs(snapshot_folder, exist_ok=True)

        # Data for DataFrame
        data = []

        for device in self.testbed.devices.values():
            if not device.connected:
                continue

            # Take the new snapshot
            interface_data = self.get_interface_counters(device)
            snapshot2_path = os.path.join(snapshot_folder, f"{device.name}_{snapshot2}")
            with open(snapshot2_path, 'w') as f:
                json.dump(interface_data, f, indent=2)
            print(f"New snapshot {snapshot2} taken for {device.name}.")

            # Load and compare values from the old and new snapshots
            if snapshot1:
                snapshot1_path = os.path.join(snapshot_folder, snapshot1)
                if os.path.exists(snapshot1_path):
                    try:
                        with open(snapshot1_path, 'r') as f:
                            data1 = json.load(f)
                        with open(snapshot2_path, 'r') as f:
                            data2 = json.load(f)

                        for interface in data1:
                            old_counters = data1[interface].get("counters", {}).get("netstat", {})
                            new_counters = data2[interface].get("counters", {}).get("netstat", {})

                            old_rx_drop = old_counters.get("rx-drop", 0)
                            new_rx_drop = new_counters.get("rx-drop", 0)
                            old_rx_error = old_counters.get("rx-error", 0)
                            new_rx_error = new_counters.get("rx-error", 0)
                            old_tx_drop = old_counters.get("tx-drop", 0)
                            new_tx_drop = new_counters.get("tx-drop", 0)
                            old_tx_error = old_counters.get("tx-error", 0)
                            new_tx_error = new_counters.get("tx-error", 0)

                            rx_drop_percent = self.calculate_percentage_change(old_rx_drop, new_rx_drop)
                            rx_error_percent = self.calculate_percentage_change(old_rx_error, new_rx_error)
                            tx_drop_percent = self.calculate_percentage_change(old_tx_drop, new_tx_drop)
                            tx_error_percent = self.calculate_percentage_change(old_tx_error, new_tx_error)

                            status = "Pass" if (old_rx_drop == new_rx_drop and 
                                                old_rx_error == new_rx_error and 
                                                old_tx_drop == new_tx_drop and 
                                                old_tx_error == new_tx_error) else "Fail"

                            if status == "Fail":
                                differences_found = True

                            data.append([device.name, interface, 
                                         old_rx_drop, new_rx_drop, rx_drop_percent,
                                         old_rx_error, new_rx_error, rx_error_percent,
                                         old_tx_drop, new_tx_drop, tx_drop_percent,
                                         old_tx_error, new_tx_error, tx_error_percent,
                                         status])

                    except Exception as e:
                        self.failed(f"Error while loading snapshots for {device.name}: {str(e)}")
                else:
                    print(f"Previous snapshot {snapshot1} does not exist for {device.name}, so loading is skipped.")

        # Convert data to DataFrame
        df = pd.DataFrame(data, columns=[
            "Device", "Interface", "Old rx-drop", "New rx-drop", "rx-drop %",
            "Old rx-error", "New rx-error", "rx-error %", "Old tx-drop", "New tx-drop", "tx-drop %",
            "Old tx-error", "New tx-error", "tx-error %", "Status"
        ])
        
        # Save to Excel
        excel_path = 'interface_counters_comparison.xlsx'
        df.to_excel(excel_path, index=False)
        
        # Add charts to the Excel file
        self.add_charts_to_excel(excel_path)
        
        if differences_found:
            self.failed("Differences found in interface counters for one or more devices.")
        else:
            self.passed("No differences found in interface counters for all devices.")

    def calculate_percentage_change(self, old_value, new_value):
        if old_value == 0:
            return 100 if new_value > 0 else 0
        try:
            return round(((new_value - old_value) / old_value) * 100, 2)
        except ZeroDivisionError:
            return 0

    def add_charts_to_excel(self, excel_path):
        wb = load_workbook(excel_path)
        ws = wb.active

        # Add Bar Chart for rx-drop differences
        chart1 = BarChart()
        chart1.title = "RX Drop Differences (%)"
        chart1.y_axis.title = 'Percentage'
        chart1.x_axis.title = 'Interface'
        data = Reference(ws, min_col=5, min_row=1, max_row=ws.max_row, max_col=5)
        cats = Reference(ws, min_col=2, min_row=2, max_row=ws.max_row)
        chart1.add_data(data, titles_from_data=True)
        chart1.set_categories(cats)
        ws.add_chart(chart1, "O2")

        # Add Bar Chart for tx-drop differences
        chart2 = BarChart()
        chart2.title = "TX Drop Differences (%)"
        chart2.y_axis.title = 'Percentage'
        chart2.x_axis.title = 'Interface'
        data = Reference(ws, min_col=11, min_row=1, max_row=ws.max_row, max_col=11)
        cats = Reference(ws, min_col=2, min_row=2, max_row=ws.max_row)
        chart2.add_data(data, titles_from_data=True)
        chart2.set_categories(cats)
        ws.add_chart(chart2, "O20")

        # Add Bar Chart for rx-error differences
        chart3 = BarChart()
        chart3.title = "RX Error Differences (%)"
        chart3.y_axis.title = 'Percentage'
        chart3.x_axis.title = 'Interface'
        data = Reference(ws, min_col=8, min_row=1, max_row=ws.max_row, max_col=8)
        cats = Reference(ws, min_col=2, min_row=2, max_row=ws.max_row)
        chart3.add_data(data, titles_from_data=True)
        chart3.set_categories(cats)
        ws.add_chart(chart3, "O38")

        # Add Bar Chart for tx-error differences
        chart4 = BarChart()
        chart4.title = "TX Error Differences (%)"
        chart4.y_axis.title = 'Percentage'
        chart4.x_axis.title = 'Interface'
        data = Reference(ws, min_col=14, min_row=1, max_row=ws.max_row, max_col=14)
        cats = Reference(ws, min_col=2, min_row=2, max_row=ws.max_row)
        chart4.add_data(data, titles_from_data=True)
        chart4.set_categories(cats)
        ws.add_chart(chart4, "O56")

        # Save the workbook
        wb.save(excel_path)

class CommonCleanup(aetest.CommonCleanup):
    @aetest.subsection
    def clean_everything(self):
        """ Common Cleanup Subsection """
        logger.info("Aetest Common Cleanup ")

if __name__ == '__main__':
    from pyats.easypy import run

    testbed_file = 'staging-testbed.yaml'
    snapshot_folder = 'snapshots'
    snapshot1 = 'snapshot1.json'  # Replace with your actual snapshot file
    snapshot2 = 'snapshot2.json'  # This will be updated in the job script

    run(testscript=__file__, testbed_file=testbed_file, snapshot_folder=snapshot_folder, snapshot1=snapshot1, snapshot2=snapshot2)
