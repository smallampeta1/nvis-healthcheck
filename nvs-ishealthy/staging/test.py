import json
from rich.console import Console
from rich.table import Table

# Sample JSON output from the command
json_output = """
{
  "bridge": {
    "domain": {
      "br_default": {
        "learning": "on"
      }
    }
  },
  "ifindex": 20,
  "ip": {
    "address": {}
  },
  "link": {
    "auto-negotiate": "off",
    "duplex": "full",
    "fec": "auto",
    "mac": "1c:34:da:15:e1:2c",
    "mtu": 9216,
    "speed": "10G",
    "state": {
      "up": {}
    },
    "stats": {
      "carrier-transitions": 2,
      "in-bytes": 605803881,
      "in-drops": 593301,
      "in-errors": 0,
      "in-pkts": 4602075,
      "out-bytes": 56433678,
      "out-drops": 0,
      "out-errors": 0,
      "out-pkts": 279834
    }
  },
  "lldp": {
    "dcbx-ets-config-tlv": "off",
    "dcbx-ets-recomm-tlv": "off",
    "dcbx-pfc-tlv": "off",
    "neighbor": {
      "MTX-DMZ-SW01": {
        "age": "7554783",
        "bridge": {
          "vlan": {}
        },
        "chassis": {
          "capability": {},
          "chassis-id": "04:3f:72:6e:d8:00",
          "system-description": "MSN2700,Onyx,SWv3.9.2504",
          "system-name": "MTX-DMZ-SW01"
        },
        "lldp-med": {
          "capability": {},
          "device-type": "",
          "inventory": {}
        },
        "port": {
          "description": " ",
          "name": "Eth1/14",
          "pmd-autoneg": {},
          "ttl": 120,
          "type": "ifname"
        }
      }
    }
  },
  "pluggable": {
    "identifier": "QSFP+",
    "vendor-name": "Mellanox",
    "vendor-pn": "MC2207130-001",
    "vendor-rev": "A3",
    "vendor-sn": "MT1549VS05104"
  },
  "type": "swp"
}
"""

# Parsing JSON output
data = json.loads(json_output)

# Extracting data
local_host = "cumulus"  # Assuming this needs to be set or determined dynamically
local_port = "swp14"
remote_host = list(data["lldp"]["neighbor"].keys())[0]  # Assuming single neighbor for simplicity
remote_port = data["lldp"]["neighbor"][remote_host]["port"]["name"]
link_speed = data["link"]["speed"]
link_state = "up" if data["link"]["state"].get("up") is not None else "down"
in_errors = data["link"]["stats"]["in-errors"]
out_errors = data["link"]["stats"]["out-errors"]
sfp_id = data["pluggable"]["identifier"]
sfp_vendor_name = data["pluggable"]["vendor-name"]
sfp_vendor_pn = data["pluggable"]["vendor-pn"]

# Displaying in table
console = Console()
table = Table(show_header=True, title="Interface Details", header_style="bold magenta")
table.add_column("LocalHost")
table.add_column("LocalPort")
table.add_column("Current RemoteHost")
table.add_column("Current RemotePort")
table.add_column("Link Speed")
table.add_column("Link State")
table.add_column("In-Errors")
table.add_column("Out-Errors")
table.add_column("SFP ID")
table.add_column("SFP Vendor-Name")
table.add_column("SFP Vendor-PN")

table.add_row(local_host, local_port, remote_host, remote_port, link_speed, link_state,
              str(in_errors), str(out_errors), sfp_id, sfp_vendor_name, sfp_vendor_pn)

console.print(table)
